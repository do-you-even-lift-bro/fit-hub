from fastapi import APIRouter, Depends, Response
from typing import Union, List
from queries.user_details import (
    Error,
    User_DetailIn,
    User_DetailOut,
    User_DetailRepository,
)

router = APIRouter()


@router.post("/user_details", response_model=Union[User_DetailOut, Error])
async def create_user_details(
    user_detail: User_DetailIn,
    response: Response,
    repo: User_DetailRepository = Depends(),
):
    new_user_detail = repo.create(user_detail)
    if new_user_detail is None:
        response.status_code = 400
    return new_user_detail


@router.get("/user_details", response_model=Union[Error, List[User_DetailOut]])
async def get_all(repo: User_DetailRepository = Depends()):
    return repo.get_all()


@router.put(
    "/user_details/{user_detail_id}",
    response_model=Union[User_DetailOut, Error],
)
async def update_user_detail(
    user_detail_id: int,
    user_detail: User_DetailIn,
    repo: User_DetailRepository = Depends(),
) -> Union[Error, User_DetailOut]:
    return repo.update(user_detail_id, user_detail)


@router.delete("/user_details/{user_detail_id}", response_model=bool)
async def delete_user_detail(
    user_detail_id: int,
    repo: User_DetailRepository = Depends(),
) -> bool:
    return repo.delete(user_detail_id)


@router.get("/user_details/{user_id}", response_model=List[User_DetailOut])
async def get_one_user_detail(
    user_id: int, response: Response, repo: User_DetailRepository = Depends()
) -> List[User_DetailOut]:
    user_detail = repo.get_one_user_detail(user_id)
    if not user_detail:
        response.status_code = 404
        return []
    return user_detail
