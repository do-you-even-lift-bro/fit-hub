from fastapi import APIRouter, Depends, Response
from typing import Union, List, Optional
from queries.workouts import (
    Error,
    WorkoutIn,
    WorkoutOut,
    WorkoutRepository,
)

router = APIRouter()


@router.post("/workouts", response_model=Union[WorkoutOut, Error])
async def create_workout(
    workout: WorkoutIn, response: Response, repo: WorkoutRepository = Depends()
):
    new_workout = repo.create(workout)
    if new_workout is None:
        response.status_code = 400
    return new_workout


@router.get("/workouts", response_model=Union[Error, List[WorkoutOut]])
async def get_all(repo: WorkoutRepository = Depends()):
    return repo.get_all()


@router.put("/workouts/{workout_id}", response_model=Union[WorkoutOut, Error])
async def update_workout(
    workout_id: int,
    workout: WorkoutIn,
    repo: WorkoutRepository = Depends(),
) -> Union[Error, WorkoutOut]:
    return repo.update(workout_id, workout)


@router.delete("/workouts/{workout_id}", response_model=bool)
async def delete_workout(
    workout_id: int,
    repo: WorkoutRepository = Depends(),
) -> bool:
    return repo.delete(workout_id)


@router.get("/workouts/{workout_id}", response_model=Optional[WorkoutOut])
async def get_one_workout(
    workout_id: int,
    response: Response,
    repo: WorkoutRepository = Depends(),
) -> WorkoutOut:
    workout = repo.get_one_workout(workout_id)
    if workout is None:
        response.status_code = 404
    return workout
