from fastapi import Depends, APIRouter, Response

from typing import List, Optional, Union
from pydantic import ValidationError
from queries.meals import MealIn, MealOut, MealRepository, Error

router = APIRouter()


@router.get("/meals", response_model=List[MealOut])
async def get_meals(meals: MealRepository = Depends()):
    return meals.get_all()


@router.post("/meals", response_model=Union[MealOut, Error])
def create_meal(
    meal: MealIn,
    response: Response,
    repo: MealRepository = Depends(),
):
    try:
        return repo.create(meal)
    except ValidationError:
        response.status_code = 422
        return Error(message="Validation error. Check request payload.")


@router.get("/meals/{meal_id}", response_model=Optional[MealOut])
def get_one_meal(
    meal_id: int,
    response: Response,
    repo: MealRepository = Depends(),
) -> MealOut:
    meal = repo.get_one(meal_id)
    if meal is None:
        response.status_code = 404
    return meal


@router.put("/meals/{meal_id}", response_model=Union[MealOut, Error])
def update_meal(
    meal_id: int,
    meal: MealIn,
    repo: MealRepository = Depends(),
) -> Union[Error, MealOut]:
    return repo.update(meal_id, meal)


@router.delete("/meals/{meal_id}", response_model=bool)
def delete_meal(
    meal_id: int,
    repo: MealRepository = Depends(),
) -> bool:
    return repo.delete(meal_id)
