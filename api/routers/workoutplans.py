from fastapi import APIRouter, status
from pydantic import BaseModel, Field
from typing import List
import os
from psycopg_pool import ConnectionPool

router = APIRouter()

pool = ConnectionPool(conninfo=os.environ["DATABASE_URL"])


class WorkoutPlanIn(BaseModel):
    name: str = Field(..., max_length=100)
    description: str = Field(..., max_length=255)


class WorkoutPlanOut(BaseModel):
    id: int
    name: str = Field(..., max_length=100)
    description: str = Field(..., max_length=255)

    class Config:
        orm_mode = True


class WorkoutPlanRepository:
    def get_all(self) -> List[WorkoutPlanOut]:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT id, name, description
                    FROM workout_plans
                    """
                )
                result = cur.fetchall()
                return [
                    WorkoutPlanOut(
                        id=row[0],
                        name=row[1],
                        description=row[2],
                    )
                    for row in result
                ]

    def create(self, workout_plan_in: WorkoutPlanIn) -> WorkoutPlanOut:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    INSERT INTO workout_plans (name, description)
                    VALUES (%s, %s)
                    RETURNING id
                    """,
                    (
                        workout_plan_in.name,
                        workout_plan_in.description,
                    ),
                )
                workout_plan_id = cur.fetchone()[0]
                return WorkoutPlanOut(
                    id=workout_plan_id, **workout_plan_in.dict()
                )

    def delete(self, workout_plan_id: int):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    DELETE FROM workout_plans
                    WHERE id = %s
                    """,
                    (workout_plan_id,),
                )


workout_plan_repo = WorkoutPlanRepository()


@router.get("/workout_plans/", response_model=List[WorkoutPlanOut])
def get_all_workout_plans():
    return workout_plan_repo.get_all()


@router.post(
    "/workout_plans/",
    response_model=WorkoutPlanOut,
    status_code=status.HTTP_201_CREATED,
)
def create_workout_plan(workout_plan_in: WorkoutPlanIn):
    return workout_plan_repo.create(workout_plan_in)


@router.delete(
    "/workout_plans/{workout_plan_id}", status_code=status.HTTP_204_NO_CONTENT
)
def delete_workout_plan(workout_plan_id: int):
    workout_plan_repo.delete(workout_plan_id)
