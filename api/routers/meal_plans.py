from fastapi import APIRouter, status
from pydantic import BaseModel, Field
from typing import List
import os
from psycopg_pool import ConnectionPool

router = APIRouter()

pool = ConnectionPool(conninfo=os.environ["DATABASE_URL"])


class MealPlanIn(BaseModel):
    title: str = Field(..., max_length=100)


class MealPlanOut(BaseModel):
    id: int
    title: str = Field(..., max_length=100)

    class Config:
        orm_mode = True


class MealPlanRepository:
    def get_all(self) -> List[MealPlanOut]:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT id, title
                    FROM meal_plan
                    """
                )
                result = cur.fetchall()
                return [MealPlanOut(id=row[0], title=row[1]) for row in result]

    def create(self, meal_plan_in: MealPlanIn) -> MealPlanOut:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    INSERT INTO meal_plan (title)
                    VALUES (%s)
                    RETURNING id
                    """,
                    (meal_plan_in.title,),
                )
                meal_plan_id = cur.fetchone()[0]
                return MealPlanOut(id=meal_plan_id, **meal_plan_in.dict())

    def delete(self, meal_plan_id: int):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    DELETE FROM meal_plan
                    WHERE id = %s
                    """,
                    (meal_plan_id,),
                )


meal_plan_repo = MealPlanRepository()


@router.get("/meal_plans/", response_model=List[MealPlanOut])
def get_all_meal_plans():
    return meal_plan_repo.get_all()


@router.post(
    "/meal_plans/",
    response_model=MealPlanOut,
    status_code=status.HTTP_201_CREATED,
)
def create_meal_plan(meal_plan_in: MealPlanIn):
    return meal_plan_repo.create(meal_plan_in)


@router.delete(
    "/meal_plans/{meal_plan_id}", status_code=status.HTTP_204_NO_CONTENT
)
def delete_meal_plan(meal_plan_id: int):
    meal_plan_repo.delete(meal_plan_id)
