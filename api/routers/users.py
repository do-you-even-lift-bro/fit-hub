from fastapi import (
    Depends,
    HTTPException,
    status,
    Response,
    APIRouter,
    Request,
)
from typing import Union, List, Optional
from queries.users import (
    Error,
    UserIn,
    UserOut,
    UserRepository,
    DuplicateAccountError,
)
from jwtdown_fastapi.authentication import Token
from authenticator import authenticator
from pydantic import BaseModel


class UserForm(BaseModel):
    username: str
    password: str


class UserToken(Token):
    account: UserOut


class HttpError(BaseModel):
    detail: str


router = APIRouter()


@router.get("/token", response_model=UserToken | None)
async def get_token(
    request: Request,
    user: UserOut = Depends(authenticator.try_get_current_account_data),
) -> UserToken | None:
    if authenticator.cookie_name in request.cookies:
        return {
            "access_token": request.cookies[authenticator.cookie_name],
            "type": "Bearer",
            "account": user,
        }
    else:
        return None


@router.post("/users", response_model=UserToken | HttpError)
async def create_user(
    user: UserIn,
    request: Request,
    response: Response,
    repo: UserRepository = Depends(),
):
    hashed_password = authenticator.hash_password(user.password)
    try:
        account = repo.create(user, hashed_password)

    except DuplicateAccountError:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Account already exists",
        )
    form = UserForm(username=user.email, password=user.password)
    token = await authenticator.login(response, request, form, repo)
    return UserToken(account=account, **token.dict())


@router.get("/users", response_model=Union[Error, List[UserOut]])
async def get_all(repo: UserRepository = Depends()):
    return repo.get_all()


@router.put("/users/{user_id}", response_model=Union[UserOut, Error])
async def update_user(
    user_id: int,
    user: UserIn,
    repo: UserRepository = Depends(),
) -> Union[Error, UserOut]:
    return repo.update(user_id, user)


@router.delete("/users/{user_id}", response_model=bool)
async def delete_user(
    user_id: int,
    repo: UserRepository = Depends(),
) -> bool:
    return repo.delete(user_id)


@router.get("/users/{user_id}", response_model=Optional[UserOut])
async def get_one_user(
    user_id: str,
    response: Response,
    repo: UserRepository = Depends(),
) -> UserOut:
    # Check if the identifier can be converted to an integer (assuming user ID)
    try:
        user_id = int(user_id)
        user = repo.get_one_user(user_id)
    except ValueError:
        # If not, assume it's an email and try to fetch the user by email
        user = repo.get_user_by_email(user_id)

    if user is None:
        raise HTTPException(status_code=404, detail="User not found")

    return user
