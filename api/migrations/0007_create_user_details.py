steps = [
    [
        """
        CREATE TABLE user_details (
            id SERIAL PRIMARY KEY NOT NULL,
            user_id INTEGER NOT NULL,
            weight INTEGER NOT NULL,
            progress_pic VARCHAR(1000),
            created_at VARCHAR(1000),
            FOREIGN KEY (user_id) REFERENCES users(id)
        );
        """,
        """
        DROP TABLE user_details;
        """
    ]
]
