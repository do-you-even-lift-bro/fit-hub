steps = [
    [
        """
        CREATE TABLE meals (
            id SERIAL NOT NULL PRIMARY KEY,
            title VARCHAR(100) NOT NULL,
            total_calories INTEGER NOT NULL,
            recipe_url VARCHAR(500) NOT NULL,
            meal_plan_id INTEGER,
            FOREIGN KEY (meal_plan_id) REFERENCES meal_plan(id)
        );
        """,
        """
        DROP TABLE meals;
        """
    ]
]
