steps = [
    [
        """
        CREATE TABLE workout_plans (
            id SERIAL NOT NULL PRIMARY KEY,
            name VARCHAR(100) NOT NULL,
            description VARCHAR(255) NOT NULL
        );
        """,
        """
        DROP TABLE workout_plans;
        """
    ]
]