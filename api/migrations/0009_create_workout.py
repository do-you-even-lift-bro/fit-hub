steps = [
    [
        """
        CREATE TABLE workouts (
            id SERIAL NOT NULL PRIMARY KEY,
            difficulty VARCHAR(100) NOT NULL,
            name VARCHAR(100) NOT NULL,
            description VARCHAR(100) NOT NULL,
            muscle_group VARCHAR(100) NOT NULL,
            duration INTEGER NOT NULL,
            reps INTEGER NOT NULL,
            workout_plan_id INTEGER NOT NULL,
            FOREIGN KEY (workout_plan_id) REFERENCES workout_plans(id) ON DELETE CASCADE
        );
        """,
        """
        DROP TABLE workouts;
        """
    ]
]
