steps = [
    [
        """
        CREATE TABLE exercises (
            id SERIAL NOT NULL PRIMARY KEY,
            name VARCHAR(100) NOT NULL,
            instructions VARCHAR(100) NOT NULL,
            video_url VARCHAR(355) NOT NULL
        );
        """,
        """
        DROP TABLE exercises;
        """
    ]
]
