steps = [
    [
        """
        CREATE TABLE meal_plan (
            id SERIAL NOT NULL PRIMARY KEY,
            title VARCHAR(100) NOT NULL
        )
        """,
        """
        DROP TABLE meal_plan;
        """
    ]
]
