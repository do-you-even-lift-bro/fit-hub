from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from routers import (
    meal_plans,
    meals,
    users,
    user_details,
    workouts,
    workoutplans,
)
from authenticator import authenticator

app = FastAPI()
app.include_router(workoutplans.router)
app.include_router(meal_plans.router)
app.include_router(meals.router)
app.include_router(users.router)
app.include_router(workouts.router)
app.include_router(authenticator.router)
app.include_router(user_details.router)

origins = [
    "http://localhost",
    "http://localhost:8000",
    "http://localhost:5173",
    "https://do-you-even-lift-bro.gitlab.io/fit-hub",
    "https://do-you-even-lift-bro.gitlab.io",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/")
def root():
    return {"message": "You hit the root path!"}
