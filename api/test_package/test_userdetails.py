from fastapi.testclient import TestClient
from main import app
from queries.user_details import User_DetailRepository

client = TestClient(app)


class EmptyUserDetailQueries:
    def get_all(self):
        return []


class MockedUserDetailQueries:
    def get_all(self):
        return [
            {
                "id": 1,
                "user_id": 1,
                "weight": 70,
                "progress_pic": None,
                "created_at": "2022-01-01 12:00:00",
            },
            {
                "id": 2,
                "user_id": 2,
                "weight": 65,
                "progress_pic": "example.jpg",
                "created_at": "2022-01-02 12:00:00",
            },
        ]


class LargeUserDetailQueries:
    def get_all(self):
        user_details = []
        for i in range(10000):
            user_details.append(
                {
                    "id": i + 1,
                    "user_id": i + 1,
                    "weight": 170,
                    "progress_pic": None,
                    "created_at": "2022-01-01 12:00:0{i}",
                }
            )
        return user_details


def test_get_all_user_details_empty():
    app.dependency_overrides[User_DetailRepository] = (
        lambda: EmptyUserDetailQueries()
    )

    response = client.get("/user_details")
    assert response.status_code == 200
    assert response.json() == []


def test_get_all_user_details():
    app.dependency_overrides[User_DetailRepository] = (
        lambda: MockedUserDetailQueries()
    )

    response = client.get("/user_details")
    assert response.status_code == 200
    assert response.json() == [
        {
            "id": 1,
            "user_id": 1,
            "weight": 70,
            "progress_pic": None,
            "created_at": "2022-01-01 12:00:00",
        },
        {
            "id": 2,
            "user_id": 2,
            "weight": 65,
            "progress_pic": "example.jpg",
            "created_at": "2022-01-02 12:00:00",
        },
    ]


def test_get_all_user_details_large():
    app.dependency_overrides[User_DetailRepository] = (
        lambda: LargeUserDetailQueries()
    )
    response = client.get("user_details")
    assert response.status_code == 200
    assert len(response.json()) == 10000
