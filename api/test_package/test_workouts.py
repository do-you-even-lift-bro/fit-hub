from fastapi.testclient import TestClient
from queries.workouts import WorkoutRepository
from main import app

client = TestClient(app)


class EmptyWorkoutQueries:
    def get_all(self):
        return []


def test_init():
    assert True


class CreateWorkoutRepository:
    def create(self, user):
        return (
            {
                "workout_plan_id": 10,
                "difficulty": "NaN",
                "name": "Something",
                "description": "string",
                "muscle_group": "string",
                "duration": 10,
                "reps": 10,
            },
        )


class LargeWorkoutQueries:
    def get_all(self):
        workout_plans = []
        for w in range(1000):
            workout_plans.append({
                "id": w + 1,
                "workout_plan_id": w + 1,
                "difficulty": "easy",
                "name": f"something {w + 1}",
                "description": "string",
                "muscle_group": "string",
                "duration": 10,
                "reps": 10,
            })
        return workout_plans


def test_get_all_large_workout_data():
    app.dependency_overrides[WorkoutRepository] = lambda: LargeWorkoutQueries()

    response = client.get("/workouts")
    assert response.status_code == 200
    assert len(response.json()) == 1000


def test_get_all_workouts():
    # Arrange
    app.dependency_overrides[WorkoutRepository] = lambda: EmptyWorkoutQueries()

    response = client.get("/workouts")
    assert response.status_code == 200
    assert response.json() == []
