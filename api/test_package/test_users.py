from fastapi.testclient import TestClient
from queries.users import UserRepository
from main import app

client = TestClient(app)


class EmptyUserQueries:
    def get_all(self):
        return []


def test_get_all_users():
    # Arrange
    app.dependency_overrides[UserRepository] = lambda: EmptyUserQueries()

    response = client.get("/users")
    assert response.status_code == 200
    assert response.json() == []
