from fastapi.testclient import TestClient
from main import app
from queries.meals import MealRepository

client = TestClient(app)


class EmptyMealQueries:
    def get_all(self):
        return []


class CreateMealQueries:
    def create(self, meal):
        return {
            "id": 1,
            "title": meal.title,
            "total_calories": meal.total_calories,
            "recipe_url": meal.recipe_url,
            "meal_plan_id": meal.meal_plan_id,
        }


def test_get_all_meals_empty():
    app.dependency_overrides[MealRepository] = lambda: EmptyMealQueries()

    response = client.get("/meals")
    assert response.status_code == 200
    assert response.json() == []


def test_create_meal():
    mock_meal = {
        "title": "Test Meal",
        "total_calories": 600,
        "recipe_url": "https://www.example.com",
        "meal_plan_id": 1,
    }

    app.dependency_overrides[MealRepository] = lambda: CreateMealQueries()
    response = client.post("/meals", json=mock_meal)
    assert response.status_code == 200
    assert response.json() == {
        "id": 1,
        "title": "Test Meal",
        "total_calories": 600,
        "recipe_url": "https://www.example.com",
        "meal_plan_id": 1,
    }
