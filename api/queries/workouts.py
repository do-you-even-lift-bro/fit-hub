from pydantic import BaseModel, Field
from typing import List, Union, Optional
from queries.pool import pool


class Error(BaseModel):
    message: str


class WorkoutIn(BaseModel):
    workout_plan_id: int = Field(..., gt=0)
    difficulty: str
    name: str
    description: str
    muscle_group: str
    duration: int
    reps: int


class WorkoutOut(BaseModel):
    id: int
    workout_plan_id: int
    difficulty: str
    name: str
    description: str
    muscle_group: str
    duration: int
    reps: int

    class Config:
        orm_mode = True
        allow_population_by_field_name = True


class WorkoutRepository:
    def get_one_workout(self, workout_id: int) -> Optional[WorkoutOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT
                        id,
                        difficulty,
                        name,
                        description,
                        muscle_group,
                        duration,
                        reps,
                        workout_plan_id
                        FROM workouts
                        WHERE id = %s
                        """,
                        [workout_id],
                    )
                    record = db.fetchone()
                    if record is None:
                        return None
                    return self.record_to_workout_out(record)
        except Exception:
            return Error(message="Could not get that workout")

    def delete(self, workout_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM workouts
                        WHERE id = %s
                        """,
                        [workout_id],
                    )
                    return True
        except Exception:
            return False

    def update(
        self, workout_id: int, workout: WorkoutIn
    ) -> Union[Error, WorkoutOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE workouts
                        SET difficulty = %s,
                            name = %s,
                            description = %s,
                            muscle_group = %s,
                            duration = %s,
                            reps = %s,
                            workout_plan_id = %s
                        WHERE id = %s
                        """,
                        [
                            workout.difficulty,
                            workout.name,
                            workout.description,
                            workout.muscle_group,
                            workout.duration,
                            workout.reps,
                            workout.workout_plan_id,
                            workout_id,
                        ],
                    )
                    return self.workout_in_to_out(workout_id, workout)
        except Exception as e:
            return Error(message=f"Could not update workout: {str(e)}")

    def get_all(self) -> Union[Error, List[WorkoutOut]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT
                            id,
                            difficulty,
                            name,
                            description,
                            muscle_group,
                            duration,
                            reps,
                            workout_plan_id
                        FROM workouts
                        ORDER BY id;
                        """
                    )
                    records = db.fetchall()
                    return [
                        self.record_to_workout_out(record)
                        for record in records
                    ]
        except Exception as e:
            return Error(message=f"Could not get all workouts: {str(e)}")

    def create(self, workout: WorkoutIn) -> Union[Error, WorkoutOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        INSERT INTO workouts
                            (difficulty,
                            name,
                            description,
                            muscle_group,
                            duration,
                            reps,
                            workout_plan_id)
                        VALUES (%s, %s, %s, %s, %s, %s, %s)
                        RETURNING id;
                        """,
                        [
                            workout.difficulty,
                            workout.name,
                            workout.description,
                            workout.muscle_group,
                            workout.duration,
                            workout.reps,
                            workout.workout_plan_id,
                        ],
                    )
                    id = db.fetchone()[0]
                    return self.workout_in_to_out(id, workout)
        except Exception as e:
            return Error(message=f"Could not create workout: {str(e)}")

    def workout_in_to_out(self, id: int, workout: WorkoutIn) -> WorkoutOut:
        return WorkoutOut(id=id, **workout.dict())

    def record_to_workout_out(self, record) -> WorkoutOut:
        return WorkoutOut(
            id=record[0],
            difficulty=record[1],
            name=record[2],
            description=record[3],
            muscle_group=record[4],
            duration=record[5],
            reps=record[6],
            workout_plan_id=record[7],
        )
