from pydantic import BaseModel, Field
from typing import List, Optional
from queries.pool import pool


class Error(BaseModel):
    message: str


class WorkoutPlanIn(BaseModel):
    name: str = Field(..., max_length=100)
    description: str = Field(..., max_length=255)


class WorkoutPlanOut(BaseModel):
    id: int
    name: str
    description: str

    class Config:
        orm_mode = True


class WorkoutPlanRepository:
    def get_one_workout_plan(
        self, workout_plan_id: int
    ) -> Optional[WorkoutPlanOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT id, name, description
                        FROM workout_plans
                        WHERE id = %s
                    """,
                        [workout_plan_id],
                    )
                    record = db.fetchone()
                    if record is None:
                        return None
                    return WorkoutPlanOut(
                        id=record[0], name=record[1], description=record[2]
                    )
        except Exception as e:
            return Error(message=f"Could not get workout plan: {e}")

    def delete_workout_plan(self, workout_plan_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM workout_plans
                        WHERE id = %s
                    """,
                        [workout_plan_id],
                    )
                    return db.rowcount > 0
        except Exception:
            return False

    def update_workout_plan(
        self, workout_plan_id: int, workout_plan: WorkoutPlanIn
    ) -> Optional[WorkoutPlanOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE workout_plans
                        SET name = %s, description = %s
                        WHERE id = %s
                        RETURNING id, name, description;
                    """,
                        [
                            workout_plan.name,
                            workout_plan.description,
                            workout_plan_id,
                        ],
                    )
                    record = db.fetchone()
                    return (
                        WorkoutPlanOut(
                            id=record[0], name=record[1], description=record[2]
                        )
                        if record
                        else None
                    )
        except Exception as e:
            return Error(message=f"Could not update workout plan: {e}")

    def get_all_workout_plans(self) -> List[WorkoutPlanOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT id, name, description
                        FROM workout_plans
                        ORDER BY id;
                    """
                    )
                    records = db.fetchall()
                    return [
                        WorkoutPlanOut(
                            id=record[0], name=record[1], description=record[2]
                        )
                        for record in records
                    ]
        except Exception as e:
            return Error(message=f"Could not get all workout plans: {e}")

    def create_workout_plan(
        self, workout_plan: WorkoutPlanIn
    ) -> Optional[WorkoutPlanOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        INSERT INTO workout_plans (name, description)
                        VALUES (%s, %s)
                        RETURNING id, name, description;
                    """,
                        [workout_plan.name, workout_plan.description],
                    )
                    record = db.fetchone()
                    return (
                        WorkoutPlanOut(
                            id=record[0], name=record[1], description=record[2]
                        )
                        if record
                        else None
                    )
        except Exception as e:
            return Error(message=f"Could not create workout plan: {e}")
