import os
from pydantic import BaseModel
from psycopg_pool import ConnectionPool
from typing import List, Union, Optional

pool = ConnectionPool(conninfo=os.environ["DATABASE_URL"])


def add(a, b):
    return a + b


class Error(BaseModel):
    message: str


class DuplicateAccountError(ValueError):
    pass


class UserIn(BaseModel):
    email: str
    password: str
    first_name: str
    last_name: str


class UserOut(BaseModel):
    id: int
    email: str
    first_name: str
    last_name: str


class UserOutWithPassword(UserOut):
    hashed_password: str


class UserRepository:
    def create(
        self, user: UserIn, hashed_password: str
    ) -> UserOutWithPassword:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO users (
                            email,
                            hashed_password,
                            first_name,
                            last_name
                        )
                        VALUES (%s, %s, %s, %s)
                        RETURNING *;
                        """,
                        [
                            user.email,
                            hashed_password,
                            user.first_name,
                            user.last_name,
                        ],
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    data = self.record_to_user_out(record).dict()
                    return UserOutWithPassword(**data)
        except Exception:
            return {"message": "Could not create user"}

    def get_one_user(self, email: str) -> Optional[UserOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT
                        *
                        FROM users
                        WHERE email = %s;
                        """,
                        [email],
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    data = self.record_to_user_out(record).dict()
                    return UserOutWithPassword(**data)
        except Exception as e:
            print(f"Error getting user: {e}")
            return None

    def get_user_by_email(self, email: str) -> Optional[UserOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT
                        *
                        FROM users
                        WHERE email = %s;
                        """,
                        [email],
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    data = self.record_to_user_out(record).dict()
                    return UserOutWithPassword(**data)
        except Exception:
            return None

    def delete(self, user_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM users
                        WHERE id = %s
                        """,
                        [user_id],
                    )
                    return True
        except Exception:
            return False

    def update(self, user_id: int, user: UserIn) -> Optional[UserOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE users
                        SET email = %s,
                            hashed_password = %s,
                            first_name = %s,
                            last_name = %s
                        WHERE id = %s;

                        """,
                        [
                            user.email,
                            user.password,
                            user.first_name,
                            user.last_name,
                            user_id,
                        ],
                    )
                    return self.get_one_user(user_id)
        except Exception:
            return None

    def get_all(self) -> Union[Error, List[UserOut]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT
                        id,
                        email,
                        hashed_password,
                        first_name,
                        last_name
                        FROM users
                        ORDER BY id;
                        """
                    )
                    users = [
                        UserOut(
                            id=record[0],
                            email=record[1],
                            hashed_password=record[2],
                            first_name=record[3],
                            last_name=record[4],
                        )
                        for record in db.fetchall()
                    ]
                    return users
        except Exception:
            return None

    def user_in_to_out(self, id: int, user: UserIn):
        old_data = user.dict()
        return UserOut(id=id, **old_data)

    def record_to_user_out(self, record):
        return UserOutWithPassword(
            id=record[0],
            email=record[1],
            hashed_password=record[2],
            first_name=record[3],
            last_name=record[4],
        )
