from pydantic import BaseModel, Field
from typing import List
import os
from psycopg_pool import ConnectionPool

pool = ConnectionPool(conninfo=os.environ["DATABASE_URL"])


class MealPlanIn(BaseModel):
    title: str = Field(..., max_length=100)


class MealPlanOut(BaseModel):
    id: int
    title: str = Field(..., max_length=100)


class Config:
    orm_mode = True


class MealPlanRepository:
    def get_all(self) -> List[MealPlanOut]:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute("SELECT id, title FROM meal_plan")
                result = cur.fetchall()
                return [MealPlanOut(id=row[0], title=row[1]) for row in result]
