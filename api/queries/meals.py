from pydantic import BaseModel, Field, HttpUrl
from typing import List, Optional, Union
import os
from psycopg_pool import ConnectionPool

pool = ConnectionPool(conninfo=os.environ["DATABASE_URL"])


class Error(BaseModel):
    message: str


class MealIn(BaseModel):
    title: str
    total_calories: int
    recipe_url: HttpUrl
    meal_plan_id: Optional[int] = Field(
        None, description="The ID of the associated meal plan"
    )


class MealOut(BaseModel):
    id: int
    title: str = Field(..., max_length=100)
    total_calories: int
    recipe_url: HttpUrl
    meal_plan_id: Optional[int] = Field(
        None, description="The ID of the associated meal plan"
    )

    class Config:
        orm_mode = True


class MealRepository:
    def get_all(self) -> List[MealOut]:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT id, title, total_calories, recipe_url, meal_plan_id
                    FROM meals
                    """
                )
                result = cur.fetchall()
                return [
                    MealOut(
                        **dict(
                            zip([column[0] for column in cur.description], row)
                        )
                    )
                    for row in result
                ]

    def get_one(self, meal_id: int) -> Optional[MealOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT
                        id,
                        title,
                        total_calories,
                        recipe_url,
                        meal_plan_id
                        FROM meals
                        WHERE id = %s
                        """,
                        [meal_id],
                    )
                    record = db.fetchone()
                    if record is None:
                        return None
                    return MealOut(
                        **dict(
                            zip(
                                [column[0] for column in db.description],
                                record,
                            )
                        )
                    )
        except Exception as e:
            return Error(message=f"Could not get that meal: {str(e)}")

    def update(self, meal_id: int, meal: MealIn) -> Union[MealOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE meals
                        SET
                        title = %s,
                        total_calories = %s,
                        recipe_url = %s,
                        meal_plan_id = %s
                        WHERE id = %s
                        RETURNING
                        id,
                        title,
                        total_calories,
                        recipe_url,
                        meal_plan_id;
                        """,
                        [
                            meal.title,
                            meal.total_calories,
                            meal.recipe_url,
                            meal.meal_plan_id,
                            meal_id,
                        ],
                    )
                    record = db.fetchone()
                    return MealOut(
                        **dict(
                            zip(
                                [column[0] for column in db.description],
                                record,
                            )
                        )
                    )
        except Exception as e:
            return Error(message=f"Could not update that meal: {str(e)}")

    def delete(self, meal_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM meals
                        WHERE id = %s
                        """,
                        [meal_id],
                    )
                    return True
        except Exception:
            return False

    def create(self, meal: MealIn) -> Union[MealOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        INSERT INTO meals (
                            title,
                            total_calories,
                            recipe_url,
                            meal_plan_id
                            )
                        VALUES (%s, %s, %s, %s)
                        RETURNING
                        id,
                        title,
                        total_calories,
                        recipe_url,
                        meal_plan_id;
                        """,
                        [
                            meal.title,
                            meal.total_calories,
                            meal.recipe_url,
                            meal.meal_plan_id,
                        ],
                    )
                    record = db.fetchone()
                    return MealOut(
                        **dict(
                            zip(
                                [column[0] for column in db.description],
                                record,
                            )
                        )
                    )
        except Exception as e:
            return Error(message=f"Create meal did not work: {str(e)}")
