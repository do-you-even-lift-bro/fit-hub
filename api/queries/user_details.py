from pydantic import BaseModel
from typing import List, Optional
from queries.pool import pool


class Error(BaseModel):
    message: str


class User_DetailIn(BaseModel):
    user_id: int
    weight: int
    progress_pic: Optional[str]
    created_at: str


class User_DetailOut(BaseModel):
    id: int
    user_id: int
    weight: int
    progress_pic: Optional[str]
    created_at: str


class User_DetailRepository:
    def get_one_user_detail(self, user_id: int) -> Optional[User_DetailOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT id, user_id, weight, progress_pic, created_at
                        FROM user_details
                        WHERE user_id = %s
                        """,
                        [user_id],
                    )
                    records = db.fetchall()
                    if records is None:
                        return None
                    data = [
                        self.record_to_user_detail_out((record))
                        for record in records
                    ]
                    print(data)
                    return data
        except Exception:
            return []

    def delete(self, user_detail_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM user_details
                        WHERE id = %s
                        """,
                        [user_detail_id],
                    )
                    conn.commit()
                    return True
        except Exception:
            return False

    def update(
        self, user_detail_id: int, user_detail: User_DetailIn
    ) -> Optional[User_DetailOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE user_details
                        SET
                        weight = %s,
                        progress_pic = %s
                        WHERE
                        id = %s
                        RETURNING
                        id,
                        user_id,
                        weight,
                        progress_pic,
                        created_at;
                        """,
                        [
                            user_detail.weight,
                            user_detail.progress_pic,
                            user_detail_id,
                            user_detail.user_id,
                        ],
                    )
                    record = db.fetchone()
                    conn.commit()
                    if record:
                        return self.record_to_user_detail_out(record)
        except Exception:
            return None

    def get_all(self) -> List[User_DetailOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT
                        id,
                        user_id,
                        weight,
                        progress_pic,
                        created_at
                        FROM user_details
                        ORDER BY id;
                        """
                    )
                    records = db.fetchall()
                    print(records)
                    return [
                        self.record_to_user_detail_out(record)
                        for record in records
                    ]
        except Exception:
            return []

    def create(self, user_detail: User_DetailIn) -> Optional[User_DetailOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        INSERT INTO user_details (
                            user_id,
                            weight,
                            progress_pic,
                            created_at
                            )
                        VALUES (%s, %s, %s, %s)
                        RETURNING
                        id,
                        user_id,
                        weight,
                        progress_pic,
                        created_at;
                        """,
                        [
                            user_detail.user_id,
                            user_detail.weight,
                            user_detail.progress_pic,
                            user_detail.created_at,
                        ],
                    )
                    record = db.fetchone()
                    conn.commit()
                    if record:
                        return self.record_to_user_detail_out(record)
        except Exception:
            return None

    def record_to_user_detail_out(self, record) -> User_DetailOut:
        return User_DetailOut(
            id=record[0],
            user_id=record[1],
            weight=record[2],
            progress_pic=record[3],
            created_at=record[4],
        )
