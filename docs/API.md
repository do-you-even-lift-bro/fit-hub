## API

### Users

| Action        | Method | URL                                    |
| ------------- | ------ | -------------------------------------- |
| List users    | GET    | http://localhost:8000/users            |
| Create a user | POST   | http://localhost:8000/users            |
| Delete a user | DELETE | http://localhost:8000/users/{user_id}  |
| Get one user  | GET    | http://localhost:8000/users{user_id}   |
| Update a user | PUT    | http://localhost:8000/users{user_id}   |

Input:

```
{
  "email": "string",
  "password": "string",
  "first_name": "string",
  "last_name": "string"
}
```

Output:

```
{
  "access_token": "string",
  "token_type": "Bearer",
  "account": {
    "id": 0,
    "email": "string",
    "first_name": "string",
    "last_name": "string"
  }
}
```

Once a user is created, they will receive token with authorization to navigate through other pages within the app.

### User Details

| Action              | Method | URL                                                  |
| ------------------- | ------ | --------------------------------------               |
| List user details   | GET    | http://localhost:8000/user_details                   |
| Create user details | POST   | http://localhost:8000/user_details                   |
| Delete user details | DELETE | http://localhost:8000/user_details/{user_detail_id}  |
| Get user details    | GET    | http://localhost:8000/user_details/{user_id}         |
| Update user details | PUT    | http://localhost:8000/user_details/{user_detail_id}  |

Input:

```
{
  "user_id": 0,
  "weight": 0,
  "progress_pic": "string",
  "created_at": "string"
}
```

Output:

```
{
  "id": 0,
  "user_id": 0,
  "weight": 0,
  "progress_pic": "string",
}
```

A user will be able to input their details on the dashboard to journal their progress.

### Workout Plans

| Action                | Method | URL                                                    |
| --------------------- | ------ | ------------------------------------------------------ |
| List workout plans    | GET    | http://localhost:8000/workout_plans/                   |
| Create a workout plan | POST   | http://localhost:8000/workout_plans/                   |
| Delete a workout plan | DELETE | http://localhost:8000/workout_plans/{workout_plan_id}/ |

Input:

```
{
  "name": "string",
  "description": "string",
}
```

Output:

```
{
  "id": 0,
  "name": "string",
  "description": "string",
}
```

This is to create workout plans for a user to start adding routines within.

### Workouts

| Action           | Method | URL                                          |
| ---------------- | ------ | -------------------------------------------- |
| List workouts    | GET    | http://localhost:8000/workouts/              |
| Create a workout | POST   | http://localhost:8000/workouts/              |
| Delete a workout | DELETE | http://localhost:8000/workouts/{workout_id}/ |
| Get a workout    | GET    | http://localhost:8000/workouts/{workout_id}/ |
| Update a workout | PUT    | http://localhost:8000/workouts/{workout_id}/ |

Input:

```
{
  "workout_plan_id": 0,
  "difficulty": "string",
  "name": "string",
  "description": "string",
  "muscle_group": "string",
  "duration": 0,
  "reps": 0
}
```

Output:

```
{
  "id": 0,
  "workout_plan_id": 0,
  "difficulty": "string",
  "name": "string",
  "description": "string",
  "muscle_group": "string",
  "duration": 0,
  "reps": 0
}
```

This is where a user will begin inputting their workout routines within the plans at a micro-level.

### Meal Plans

| Action             | Method | URL                                                  |
| ------------------ | ------ | ---------------------------------------------------- |
| List meal plans    | GET    | http://localhost:8000/meal_plans/                    |
| Create a meal plan | POST   | http://localhost:8000/meal_plans/                    |
| Delete a meal plan | DELETE | http://localhost:8000/api/meal_plans/{meal_plan_id}/ |

Input:

```
{
  "title": "string"
}
```

Output:

```
{
  "id": 0,
  "title": "string"
}
```

The user creates the meal plan name here.

### Meals

| Action        | Method | URL                                    |
| ------------- | ------ | -------------------------------------- |
| List meals    | GET    | http://localhost:8000/meals/           |
| Create a meal | POST   | http://localhost:8000/meals/           |
| Delete a meal | DELETE | http://localhost:8000/meals/{meal_id}/ |
| Update a meal | PUT    | http://localhost:8000/meals/{meal_id}/ |
| Get a meal    | GET    | http://localhost:8000/meals/{meal_id}/ |

Input:

```
{
  "title": "string",
  "total_calories": 0,
  "recipe_url": "string",
  "meal_plan_id": 0
}
```

Output:

```
{
  "id": 0,
  "title": "string",
  "total_calories": 0,
  "recipe_url": "string",
  "meal_plan_id": 0
}
```

This is where a user add in their specific meals with the recipe name, the amount of calories, and the url to the recipe if applicable.
