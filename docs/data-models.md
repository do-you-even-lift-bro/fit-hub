## Data Models

#### Users

##### User

| Name            | Type       | Unique | Optional |
| :-------------- | :--------- | :----- | :------- |
| id              | serial key | yes    | no       |
| email           | string     | yes    | no       |
| hashed_password | string     | no     | no       |
| first name      | string     | no     | no       |
| last name       | string     | no     | no       |

##### User Details

| Name             | Type       | Unique | Optional |
| :--------------- | :--------- | :----- | :------- |
| id               | serial key | yes    | no       |
| user_id          | integer    | yes    | no       |
| weight           | integer    | no     | no       |
| progress_pic     | string     | yes    | yes      |
| created_at       | string     | no     | yes      |

#### Workout

##### Workout Plan

| Name        | Type       | Unique | Optional |
| :---------- | :--------- | :----- | :------- |
| id          | serial key | yes    | no       |
| name        | string     | no     | no       |
| description | string     | no     | no       |

##### Workout

| Name            | Type       | Unique | Optional |
| :-------------- | :--------- | :----- | :------- |
| id              | serial key | yes    | no       |
| workout_plan_id | integer    | yes    | no       |
| difficulty      | string     | no     | no       |
| name            | string     | no     | no       |
| description     | string     | no     | no       |
| muscle_group    | string     | no     | no       |
| duration        | integer    | no     | no       |
| reps            | integer    | no     | no       |

#### Diet Planner

##### Meal Plan

| Name  | Type       | Unique | Optional |
| :---- | :--------- | :----- | :------- |
| id    | serial key | yes    | no       |
| title | string     | no     | no     |

##### Meal

| Name           | Type       | Unique | Optional |
| :------------- | :--------- | :----- | :------- |
| id             | serial key | yes    | no       |
| title          | string     | no     | no       |
| total_calories | integer    | no     | no       |
| recipe_url     | string     | no     | no       |
| meal_plan_id   | integer    | yes    | yes      |
