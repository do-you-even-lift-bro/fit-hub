![image info](https://i.imgur.com/Id7WV8w.png)

# FitHub Fitness App

This app is designed for users to signup/login and log in their workout routines and diet plans. As a user navigates through the app, they are greeted by a fully customizable dashboard with all their metrics and goals.

## Installation

-   install git, docker, and node on your device.
-   Fork the repository
-   Clone the repository via http in your terminal in your desired directory.
-   run
    -   docker volume create postgres-data
    -   docker-compose build
    -   docker-compose up

Check to make sure all containers are running and there are no errors in the logs.
You are setup!

## Authors

-   [@christopher.a.sanford](https://gitlab.com/christopher.a.sanford) - Christopher Sanford
-   [@ralphyluis44](https://gitlab.com/ralphyluis44) - Ralphy Luis
-   [@zachlubbers](https://gitlab.com/zachlubbers) - Zachary Lubbers
-   [@ascchung](https://gitlab.com/ascchung) - Andrew Chung

## Links

-   [GHI Wireframe Design](https://www.figma.com/file/0E1LYbLxsJVFHpxAfg5d2c/FitHub-Wireframe?type=design&t=2XxNMFLbHW3Qyzi7-6)
-   [API](/docs/API.md)
-   [Data Models](/docs/data-models.md)
-   [Issue Tracker](https://fithub-doyouevenliftbro.atlassian.net/jira/software/projects/FIT/boards/2)
