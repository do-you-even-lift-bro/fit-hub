03/19/2024 - Assisted with front end auth as well as some pathing issues.

03/14/2024 - End of week, overcame CORS errors while deploying frontend. Not very functional site is appearing, but it is appearing. Will track down and fix the issues either over the weekend or early next week.

03/13/2024 - Working on app deployment this week. Database seems to have deployed without problem. Still testing and working on front end deployment.

03/8/2024 - A long week it has been. The rest of the team basically completed the skeleton work of the frontend while i spent most of the week trying to fix backend authentication. Ended up requiring help from Will that was much appreciated. With the backend auth finally complete, I can now move on to something else.

03/1/2024 - _I guess I didn't save my previous entry so I will include that as well_
It's been a cold and blustery few days, hands being kept warm through diligent typing of the keyboard. Things were not looking too well, docker wasn't running, router files not working, tempers were raised. But after a curt 5 hours, we saw the light at the end of the tunnel. Our docker compose file was fixed, docker containers were up and running, and Mr. Lubbers computer did not explode. High fives all around as our team overcame our first major blocker. Many more will come but a victory is a victory. Other than that, router files have been composed and I put together a barebones auth file. Once the user functions are finalized, they can be linked. Until then.

02/26/2024 - Most of the pre planning for project Fit-Hub is complete. Wireframing facilitated by our project lead, Mr. Andrew Chung, with group participation. API Endpoints started by Mr. Ralphy Luis, also with group input. Sections of the app divided up amongst the team members.
