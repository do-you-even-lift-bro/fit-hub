## March 20 2024
    This was a very stressful yet productive week. We continued to work on styling and functionality on the frontend. We also finished all 4 unit tests and FE/BE authentication. Chris has worked tirelessly to finish deployment which is extremely close to being complete.

    The frontend authentication led us to have to redo much of the frontend components, including the login, signup, and user dashboard which were the components that I had originally worked on. It was a painful 2 day process but we came together as a team and figured it out with help from the instructors and the SEIRs.

    As of now we have one day and 2 hours and are really just working to get some last minute frontend components finished up and Chris is putting the finishing touches on the frontend deployment.
## March 15 2024
    Our group spent most of this week working on our own individual frontend pieces while Chris took charge of frontend and backend deployment.

    The bulk of my time was spent on the functionality of the login and signup features. I spent some time styling the user dashboard as well.

    This week my main focus will be joining and adjusting the existing tables to be able to accomplish things like displaying a user's points and completed workouts.

##
    There was a journal entry in here but it somehow got lost to the sands of time. I looked through all the past merges and it seems to have disappeared.

## February 26 2024
    Last week our group spent alot of time brainstorming ideas for our application. We came up with features that we wanted to implement and discussed how these features would interact with each other.

    Once we recorded the features of our application we created the wireframe for how we envisioned the website to look. Andrew did an amazing job designing the wireframe which made it easy to visualize the application.

    Once we locked down the design we assigned roles. I took up the role of creating accounts which includes logging in, signing up, and the account page which will house details of workouts and progress for the individual. I was initially going to work on the workout and diet planning but we decided that the accounts function would be important to every other function and would take priority.

    Our plan now is that accounts, workout/diet planning, blogs/articles, and the quick workout function will be assigned to team members and the calendar function will be saved for last as all the other functions will feed into it.
