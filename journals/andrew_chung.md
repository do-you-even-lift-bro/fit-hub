# 4/19/2024

-   Took the duty to look after frontend auth because we ran into a few problems and realized we were missing a few elements for it to work with the backend. After a couple of days, I believe we have pulled through and got it to work! Definitely a lot of Googling and HMU to figure out what was causing problems with token authorization.
-   I'm going to be looking over a few final necessary details before finishing up on styling the frontend.
-   Will be starting to work on Readme.md to get that started for the team.

# 4/13/2024

-   Ran into Tailwind CSS issues and had some assistance from Rosheen regarding the initial set up.
-   Soon after that, I ran into another issue with Tailwind CSS after merging. I finally thought of re-cloning the repo because other peers were able to see some styling on the frontend.
-   My next task is to continue formatting the front end while I wait for my teammates to bring the data from back to end, all the while testing our unit tests to make sure they pass whenever we attempt merge on the repo.

# 4/4/2024

Today reviewed the following:

-   We ran into some errors last Friday, causing my pushes to be delayed, but it ensured that the users.py files for queries and routers were tested and complete after my revisions. Passing the users.py now to Christopher to connect with auth.
-   My task this week is to finish profile.py and workout_plans.py files for both queries and routers. Queries may be complete, but there are some issues that need to be resolved in order for it to run properly in the backend.
-   Once those are complete, I will tag team with whoever is done to start working on front end and discover more uses for Redux with React.

# 2/26/2024

Today reviewed the following:

-   Last minute look over wire frames before review with the instructors.
-   Received role to take on the "Quick Workout" section
    -   Once I achieve the "Quick Workout" I'll jump over to the other portions of the project that is needed.
-   Added the volumes based on the Project Advice workshop.
