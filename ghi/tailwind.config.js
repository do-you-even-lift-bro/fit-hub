import flowbitePlugin from 'flowbite/plugin'

export default {
    content: [
        './index.html',
        './src/**/*.{js,ts,jsx,tsx}',
        './node_modules/flowbite/**/*.{js,jsx}',
    ],
    theme: {
        extend: {
            colors: {
                yellow: '#FFA629',
                gray: '#E0E0E0',
            },
            backgroundColor: {
                body: '#333',
            },
            backgroundSize: {
                auto: 'auto',
            },
        },
    },
    plugins: [flowbitePlugin],
}
