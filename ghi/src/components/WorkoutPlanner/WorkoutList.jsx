import { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import { Button } from 'flowbite-react'

const API_HOST = import.meta.env.VITE_API_HOST

function WorkoutList() {
    const [workouts, setWorkouts] = useState([])
    const [workoutPlans, setWorkoutPlans] = useState([])
    const [selectedWorkoutPlan, setSelectedWorkoutPlan] = useState('')
    const [editWorkoutId, setEditWorkoutId] = useState(null)
    const [editableWorkout, setEditableWorkout] = useState({})

    useEffect(() => {
        const fetchData = async () => {
            const plansResponse = await fetch(`${API_HOST}/workout_plans/`)
            const plansData = await plansResponse.json()
            if (plansResponse.ok) {
                setWorkoutPlans(plansData)
            } else {
                console.error('Failed to fetch workout plans')
            }

            const workoutsResponse = await fetch(`${API_HOST}/workouts/`)
            const workoutsData = await workoutsResponse.json()
            if (workoutsResponse.ok) {
                setWorkouts(workoutsData)
            } else {
                console.error('Failed to fetch workouts')
            }
        }

        fetchData()
    }, [])

    const handleFilterChange = (event) => {
        setSelectedWorkoutPlan(event.target.value)
    }

    const handleEditChange = (event) => {
        const { name, value } = event.target
        setEditableWorkout((prev) => ({ ...prev, [name]: value }))
    }

    const handleEditClick = (workoutId) => {
        const workoutToEdit = workouts.find(
            (workout) => workout.id === workoutId
        )
        setEditWorkoutId(workoutId)
        setEditableWorkout(workoutToEdit)
    }

    const handleSave = async () => {
        const response = await fetch(`${API_HOST}/workouts/${editWorkoutId}`, {
            method: 'PUT',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(editableWorkout),
        })
        if (response.ok) {
            const updatedWorkout = await response.json()
            setWorkouts(
                workouts.map((workout) =>
                    workout.id === editWorkoutId
                        ? { ...workout, ...updatedWorkout }
                        : workout
                )
            )
            setEditWorkoutId(null)
            setEditableWorkout({})
        } else {
            console.error('Failed to update workout')
        }
    }

    const deleteWorkout = async (workoutId) => {
        const response = await fetch(`${API_HOST}/workouts/${workoutId}`, {
            method: 'DELETE',
        })
        if (response.ok) {
            setWorkouts(workouts.filter((workout) => workout.id !== workoutId))
        } else {
            console.error('Failed to delete workout')
        }
    }

    const filteredWorkouts = selectedWorkoutPlan
        ? workouts.filter(
              (workout) =>
                  workout.workout_plan_id === Number(selectedWorkoutPlan)
          )
        : workouts

    return (
        <div
            className="p-4 bg-body bg-center bg-no-repeat text-white min-h-screen bg-[url('https://i.imgur.com/GwS5hAc.jpeg')] bg-gray-700 bg-blend-multiply"
            style={{
                backgroundSize: 'cover',
                backgroundPosition: 'center',
                backgroundAttachment: 'fixed',
            }}
        >
            <div className="mb-8">
                <h2 className="text-xl font-bold mb-4">
                    Choose a Workout Plan
                </h2>
                <select
                    value={selectedWorkoutPlan}
                    onChange={handleFilterChange}
                    className="p-2 bg-gray-800 text-white border border-gray-600 rounded"
                >
                    <option value="">All Workout Plans</option>
                    {workoutPlans.map((plan) => (
                        <option key={plan.id} value={plan.id}>
                            {plan.name}
                        </option>
                    ))}
                </select>
                <Link to="/workoutplanner">
                    <Button className="border border-yellow text-yellow hover:border-white hover:text-yellow mt-4 duration-500">
                        Create a Workout
                    </Button>
                </Link>
            </div>
            <div className="mb-8">
                <h2 className="text-xl font-bold mb-4">Workouts</h2>
                <div className="overflow-x-auto">
                    <table className="min-w-full bg-gray-800 rounded-lg overflow-hidden">
                        <thead className="bg-orange-600">
                            <tr>
                                <th className="py-2 px-4 text-left">ID</th>
                                <th className="py-2 px-4 text-left">Name</th>
                                <th className="py-2 px-4 text-left">
                                    Difficulty
                                </th>
                                <th className="py-2 px-4 text-left">
                                    Muscle Group
                                </th>
                                <th className="py-2 px-4 text-left">
                                    Duration
                                </th>
                                <th className="py-2 px-4 text-left">Reps</th>
                                <th className="py-2 px-4 text-left">
                                    Description
                                </th>
                                <th className="py-2 px-4 text-left">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            {filteredWorkouts.map((workout) =>
                                editWorkoutId === workout.id ? (
                                    <tr
                                        key={workout.id}
                                        className="border-b border-gray-700"
                                    >
                                        <td className="py-2 px-4">
                                            {editableWorkout.id}
                                        </td>
                                        <td className="py-2 px-4">
                                            <input
                                                type="text"
                                                name="name"
                                                value={editableWorkout.name}
                                                onChange={handleEditChange}
                                                className="bg-gray-700 text-black p-1 rounded"
                                            />
                                        </td>
                                        <td className="py-2 px-4">
                                            <input
                                                type="text"
                                                name="difficulty"
                                                value={
                                                    editableWorkout.difficulty
                                                }
                                                onChange={handleEditChange}
                                                className="bg-gray-700 text-black p-1 rounded"
                                            />
                                        </td>
                                        <td className="py-2 px-4">
                                            <input
                                                type="text"
                                                name="muscle_group"
                                                value={
                                                    editableWorkout.muscle_group
                                                }
                                                onChange={handleEditChange}
                                                className="bg-gray-700 text-black p-1 rounded"
                                            />
                                        </td>
                                        <td className="py-2 px-4">
                                            <input
                                                type="number"
                                                name="duration"
                                                value={editableWorkout.duration}
                                                onChange={handleEditChange}
                                                className="bg-gray-700 text-black p-1 rounded"
                                            />
                                        </td>
                                        <td className="py-2 px-4">
                                            <input
                                                type="number"
                                                name="reps"
                                                value={editableWorkout.reps}
                                                onChange={handleEditChange}
                                                className="bg-gray-700 text-black p-1 rounded"
                                            />
                                        </td>
                                        <td className="py-2 px-4">
                                            <input
                                                type="text"
                                                name="description"
                                                value={
                                                    editableWorkout.description
                                                }
                                                onChange={handleEditChange}
                                                className="bg-gray-700 text-black p-1 rounded"
                                            />
                                        </td>
                                        <td className="py-2 px-4">
                                            <button
                                                onClick={handleSave}
                                                className="bg-orange-500 hover:bg-orange-700 text-white font-bold py-1 px-2 rounded mr-2"
                                            >
                                                Save
                                            </button>
                                            <button
                                                onClick={() =>
                                                    setEditWorkoutId(null)
                                                }
                                                className="bg-gray-500 hover:bg-gray-700 text-white font-bold py-1 px-2 rounded"
                                            >
                                                Cancel
                                            </button>
                                        </td>
                                    </tr>
                                ) : (
                                    <tr
                                        key={workout.id}
                                        className="border-b border-gray-700"
                                    >
                                        <td className="py-2 px-4">
                                            {workout.id}
                                        </td>
                                        <td className="py-2 px-4">
                                            {workout.name}
                                        </td>
                                        <td className="py-2 px-4">
                                            {workout.difficulty}
                                        </td>
                                        <td className="py-2 px-4">
                                            {workout.muscle_group}
                                        </td>
                                        <td className="py-2 px-4">
                                            {workout.duration}
                                        </td>
                                        <td className="py-2 px-4">
                                            {workout.reps}
                                        </td>
                                        <td className="py-2 px-4">
                                            {workout.description}
                                        </td>
                                        <td className="py-2 px-4 flex items-center">
                                            <button
                                                onClick={() =>
                                                    handleEditClick(workout.id)
                                                }
                                                className="bg-yellow-500 hover:bg-yellow-700 text-white font-bold py-1 px-2 rounded mr-2"
                                            >
                                                Edit
                                            </button>
                                            <button
                                                onClick={() =>
                                                    deleteWorkout(workout.id)
                                                }
                                                className="bg-red-500 hover:bg-red-700 text-white font-bold py-1 px-2 rounded"
                                            >
                                                Delete
                                            </button>
                                        </td>
                                    </tr>
                                )
                            )}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    )
}

export default WorkoutList
