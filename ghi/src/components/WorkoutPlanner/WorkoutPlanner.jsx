import { useEffect, useState } from 'react'

const API_HOST = import.meta.env.VITE_API_HOST

function WorkoutPlanner() {
    const [newWorkoutPlan, setNewWorkoutPlan] = useState({
        name: '',
        description: '',
    })
    const [workoutPlans, setWorkoutPlans] = useState([])
    const [newWorkout, setNewWorkout] = useState({
        workout_plan_id: '',
        difficulty: '',
        name: '',
        description: '',
        muscle_group: '',
        duration: '',
        reps: '',
    })

    useEffect(() => {
        const fetchWorkoutPlans = async () => {
            try {
                const response = await fetch(`${API_HOST}/workout_plans/`)
                const data = await response.json()
                if (response.ok) {
                    setWorkoutPlans(data)
                } else {
                    console.error('Failed to fetch workout plans')
                }
            } catch (error) {
                console.error('Error:', error)
            }
        }
        fetchWorkoutPlans()
    }, [])

    const handleAddWorkout = async (event) => {
        event.preventDefault()
        const data = {
            ...newWorkout,
            duration: parseInt(newWorkout.duration),
            reps: parseInt(newWorkout.reps),
        }
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }
        try {
            const response = await fetch(`${API_HOST}/workouts/`, fetchConfig)
            if (response.ok) {
                await response.json()
                setNewWorkout({
                    workout_plan_id: '',
                    difficulty: '',
                    name: '',
                    description: '',
                    muscle_group: '',
                    duration: '',
                    reps: '',
                })
                alert('Workout created!')
            } else {
                console.error('Failed to create workout:', response.statusText)
            }
        } catch (error) {
            console.error('Network error:', error.message)
        }
    }

    const handleAddWorkoutPlan = async (event) => {
        event.preventDefault()
        const data = {
            name: newWorkoutPlan.name,
            description: newWorkoutPlan.description,
        }
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }
        try {
            const response = await fetch(
                `${API_HOST}/workout_plans/`,
                fetchConfig
            )
            if (response.ok) {
                const newWorkoutPlanResponse = await response.json()
                setNewWorkoutPlan({
                    name: '',
                    description: '',
                    muscle_group: '',
                })

                setWorkoutPlans([...workoutPlans, newWorkoutPlanResponse])
                alert('Workout plan created!')
            } else {
                console.error(
                    'Failed to create workout plan:',
                    response.statusText
                )
            }
        } catch (error) {
            console.error('Network error:', error.message)
        }
    }

    return (
        <div className="min-h-screen bg-gray-700 text-white p-4 flex flex-col items-center justify-start">
            <div className="flex justify-center w-full space-x-4">
                <div className="bg-gray-800 p-6 rounded-lg shadow-lg md:w-1/2">
                    <h2 className="text-2xl font-bold text-orange-500 mb-4">
                        Create a Workout Plan
                    </h2>
                    <form
                        onSubmit={handleAddWorkoutPlan}
                        className="flex flex-col space-y-4"
                    >
                        <input
                            className="p-2 bg-gray-900 text-black border border-gray-600 rounded"
                            type="text"
                            placeholder="Workout Plan Name"
                            value={newWorkoutPlan.name}
                            onChange={(e) =>
                                setNewWorkoutPlan({
                                    ...newWorkoutPlan,
                                    name: e.target.value,
                                })
                            }
                        />
                        <textarea
                            className="p-2 bg-gray-900 text-white border border-gray-600 rounded"
                            placeholder="Description"
                            value={newWorkoutPlan.description}
                            onChange={(e) =>
                                setNewWorkoutPlan({
                                    ...newWorkoutPlan,
                                    description: e.target.value,
                                })
                            }
                        />
                        <button
                            className="bg-orange-500 hover:bg-orange-600 text-black px-4 py-2 rounded"
                            type="submit"
                        >
                            Add Workout Plan
                        </button>
                    </form>
                </div>
                <div className="bg-gray-800 p-6 rounded-lg shadow-lg md:w-1/2">
                    <h2 className="text-2xl font-bold text-orange-500 mb-4">
                        Add a Workout
                    </h2>
                    <form
                        onSubmit={handleAddWorkout}
                        className="flex flex-col space-y-4"
                    >
                        <select
                            className="p-2 bg-gray-900 text-white border border-gray-600 rounded"
                            value={newWorkout.workout_plan_id}
                            onChange={(e) =>
                                setNewWorkout({
                                    ...newWorkout,
                                    workout_plan_id: e.target.value,
                                })
                            }
                        >
                            <option value="">Select Workout Plan</option>
                            {workoutPlans.map((plan) => (
                                <option key={plan.id} value={plan.id}>
                                    {plan.name}
                                </option>
                            ))}
                        </select>
                        <input
                            className="p-2 bg-gray-900 text-black border border-gray-600 rounded"
                            type="text"
                            placeholder="Difficulty"
                            value={newWorkout.difficulty}
                            onChange={(e) =>
                                setNewWorkout({
                                    ...newWorkout,
                                    difficulty: e.target.value,
                                })
                            }
                        />
                        <input
                            className="p-2 bg-gray-900 text-black border border-gray-600 rounded"
                            type="text"
                            placeholder="Workout Name"
                            value={newWorkout.name}
                            onChange={(e) =>
                                setNewWorkout({
                                    ...newWorkout,
                                    name: e.target.value,
                                })
                            }
                        />
                        <textarea
                            className="p-2 bg-gray-900 text-white border border-gray-600 rounded"
                            placeholder="Description"
                            value={newWorkout.description}
                            onChange={(e) =>
                                setNewWorkout({
                                    ...newWorkout,
                                    description: e.target.value,
                                })
                            }
                        />
                        <input
                            className="p-2 bg-gray-900 text-black border border-gray-600 rounded"
                            type="text"
                            placeholder="Muscle Group"
                            value={newWorkout.muscle_group}
                            onChange={(e) =>
                                setNewWorkout({
                                    ...newWorkout,
                                    muscle_group: e.target.value,
                                })
                            }
                        />
                        <input
                            className="p-2 bg-gray-900 text-black border border-gray-600 rounded"
                            type="number"
                            placeholder="Duration (minutes)"
                            value={newWorkout.duration}
                            onChange={(e) =>
                                setNewWorkout({
                                    ...newWorkout,
                                    duration: e.target.value,
                                })
                            }
                        />
                        <input
                            className="p-2 bg-gray-900 text-black border border-gray-600 rounded"
                            type="number"
                            placeholder="Reps"
                            value={newWorkout.reps}
                            onChange={(e) =>
                                setNewWorkout({
                                    ...newWorkout,
                                    reps: e.target.value,
                                })
                            }
                        />
                        <button
                            className="bg-orange-500 hover:bg-orange-600 text-black px-4 py-2 rounded"
                            type="submit"
                        >
                            Add Workouts
                        </button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default WorkoutPlanner
