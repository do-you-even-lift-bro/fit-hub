import { Footer, Button } from 'flowbite-react'
import { Link } from 'react-router-dom'
import FitnessIcon from './Fitnessicon'
import EatingIcon from './Eatingicon'
import GainsIcon from './Gainsicon'

const CarouselWrapper = () => {
    return (
        <div
            id="default-carousel"
            className="relative h-1/2 rounded-lg lg:h-1/2 md:h-96"
            data-carousel="static"
        >
            <div className="relative h-full overflow-hidden">
                <CarouselItems />
            </div>
            <SliderIndicators />
            <SliderControls />
        </div>
    )
}

const CarouselItems = () => {
    const images = [
        'https://i.imgur.com/MwmRX3u.jpeg',
        'https://i.imgur.com/CXxKWtD.jpeg',
        'https://i.imgur.com/hCI5gEj.jpeg',
    ]

    return (
        <>
            {images.map((image, index) => (
                <div
                    key={index}
                    className="hidden duration-700 ease-in-out"
                    data-carousel-item
                >
                    <img
                        src={image}
                        className="absolute block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2"
                        alt={`Slide ${index + 1}`}
                    />
                </div>
            ))}
        </>
    )
}

const SliderIndicators = () => {
    return (
        <div className="absolute z-30 flex -translate-x-1/2 bottom-5 left-1/2 space-x-3 rtl:space-x-reverse">
            {[...Array(3).keys()].map((index) => (
                <button
                    key={index}
                    type="button"
                    className="w-3 h-3 rounded-full"
                    aria-current={index === 0}
                    aria-label={`Slide ${index + 1}`}
                    data-carousel-slide-to={index}
                ></button>
            ))}
        </div>
    )
}

const SliderControls = () => {
    return (
        <>
            <button
                type="button"
                className="absolute top-0 start-0 z-30 flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none"
                data-carousel-prev
            >
                <span className="inline-flex items-center justify-center w-10 h-10 rounded-full bg-white/30 dark:bg-gray-800/30 group-hover:bg-white/50 dark:group-hover:bg-gray-800/60 group-focus:ring-4 group-focus:ring-white dark:group-focus:ring-gray-800/70 group-focus:outline-none">
                    <svg
                        className="w-4 h-4 text-white dark:text-gray-800 rtl:rotate-180"
                        aria-hidden="true"
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 6 10"
                    >
                        <path
                            stroke="currentColor"
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            strokeWidth="2"
                            d="M5 1 1 5l4 4"
                        />
                    </svg>
                    <span className="sr-only">Previous</span>
                </span>
            </button>
            <button
                type="button"
                className="absolute top-0 end-0 z-30 flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none"
                data-carousel-next
            >
                <span className="inline-flex items-center justify-center w-10 h-10 rounded-full bg-white/30 dark:bg-gray-800/30 group-hover:bg-white/50 dark:group-hover:bg-gray-800/60 group-focus:ring-4 group-focus:ring-white dark:group-focus:ring-gray-800/70 group-focus:outline-none">
                    <svg
                        className="w-4 h-4 text-white dark:text-gray-800 rtl:rotate-180"
                        aria-hidden="true"
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 6 10"
                    >
                        <path
                            stroke="currentColor"
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            strokeWidth="2"
                            d="m1 9 4-4-4-4"
                        />
                    </svg>
                    <span className="sr-only">Next</span>
                </span>
            </button>
        </>
    )
}

const HomePage = () => {
    return (
        <main className="bg-body justify-center items-center min-h-screen">
            <div className="h-screen">
                <CarouselWrapper />
                <div className="flex flex-wrap justify-center items-center my-10">
                    <FitnessIcon />
                    <EatingIcon />
                    <GainsIcon />
                </div>
                <h2 className="text-4xl font-bold mb-4 text-yellow text-center my-10">
                    Stay Educated
                </h2>
                <div className="flex flex-wrap justify-center text-white my-10">
                    <div className="flex flex-col justify-center items-center w-full md:w-1/3 p-4 text-center">
                        <h2 className="text-center font-bold text-xl">
                            The Benefits of Regular Exercise
                        </h2>
                        <small className="mt-2">
                            Explore the numerous physical and mental health
                            benefits that come with incorporating regular
                            exercise into your routine.
                        </small>
                        <Link to="/blogs/workout-routine">
                            <Button className="border border-yellow text-yellow hover:border-white hover:text-yellow mt-4 duration-500">
                                Read More
                            </Button>
                        </Link>
                    </div>
                    <div className="flex flex-col justify-center items-center w-full md:w-1/3 p-4 text-center">
                        <h2 className="text-center font-bold text-xl">
                            Healthy Eating = Better Life
                        </h2>
                        <small className="mt-2">
                            Discover the essentials of maintaining a healthy
                            diet in today&apos;s fast-paced world.
                        </small>
                        <Link to="/blogs/healthy-eating">
                            <Button className="border border-yellow text-yellow hover:border-white hover:text-yellow mt-4 duration-500">
                                Read More
                            </Button>
                        </Link>
                    </div>
                </div>
            </div>
            <Footer
                container
                className="bg-transparent fixed bottom-0 left-0 w-full"
            >
                <div className="w-full text-center flex flex-col">
                    <Footer.Divider />
                    <div className="flex w-full items-center justify-center">
                        <Footer.Brand
                            src="https://i.imgur.com/Id7WV8w.png"
                            alt="FitHub Logo"
                            name="FitHub"
                        />
                        <Footer.Copyright href="#" by="FitHub" year={2024} />
                    </div>
                </div>
            </Footer>
        </main>
    )
}

export default HomePage
