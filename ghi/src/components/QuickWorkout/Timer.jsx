import { useContext, useState, useEffect, useRef, useCallback } from 'react'
import { CircularProgressbar, buildStyles } from 'react-circular-progressbar'
import 'react-circular-progressbar/dist/styles.css'
import PlayButton from './PlayButton'
import PauseButton from './PauseButton'
import SettingsButton from './SettingsButton'
import SettingsContext from './SettingsContext'

const workoutColor = '#FFA629'
const restColor = '#03A9F4'

function Timer() {
    const settingsInfo = useContext(SettingsContext)

    const [isPaused, setIsPaused] = useState(true)
    const [mode, setMode] = useState('workout')
    const [secondsLeft, setSecondsLeft] = useState(0)

    const secondsLeftRef = useRef(secondsLeft)
    const isPausedRef = useRef(isPaused)
    const modeRef = useRef(mode)

    const switchMode = useCallback(() => {
        const nextMode = modeRef.current === 'workout' ? 'rest' : 'workout'
        const nextSeconds =
            (nextMode === 'workout'
                ? settingsInfo.workoutMinutes
                : settingsInfo.restMinutes) * 60

        setMode(nextMode)
        modeRef.current = nextMode

        setSecondsLeft(nextSeconds)
        secondsLeftRef.current = nextSeconds
    }, [settingsInfo])

    const tick = useCallback(() => {
        secondsLeftRef.current = secondsLeftRef.current - 1
        setSecondsLeft(secondsLeftRef.current)
    }, [])

    const initTimer = useCallback(() => {
        const initialSeconds =
            (modeRef.current === 'workout'
                ? settingsInfo.workoutMinutes
                : settingsInfo.restMinutes) * 60

        setSecondsLeft(initialSeconds)
        secondsLeftRef.current = initialSeconds
    }, [settingsInfo])

    useEffect(() => {
        initTimer()

        const interval = setInterval(() => {
            if (isPausedRef.current) {
                return
            }
            if (secondsLeftRef.current === 0) {
                return switchMode()
            }
            tick()
        }, 1000)

        return () => clearInterval(interval)
    }, [settingsInfo, initTimer, switchMode, tick])

    const totalSeconds =
        mode === 'workout'
            ? settingsInfo.workoutMinutes * 60
            : settingsInfo.restMinutes * 60

    const percentage = Math.round((secondsLeft / totalSeconds) * 100)

    const minutes = Math.floor(secondsLeft / 60)
    let seconds = secondsLeft % 60
    if (seconds < 10) seconds = '0' + seconds

return (
    <div className="flex items-center justify-center h-screen">
        <div className="text-center">
            <CircularProgressbar
                value={percentage}
                text={minutes + ':' + seconds}
                styles={buildStyles({
                    rotation: 0.25,
                    strokeLinecap: 'butt',
                    textSize: '16px',
                    pathTransitionDuration: 0.5,
                    pathColor: mode === 'workout' ? workoutColor : restColor,
                    textColor: '#d6d6d6',
                    trailColor: '#d6d6d6',
                })}
            />
            <div className="mt-4">
                {isPaused ? (
                    <PlayButton
                        onClick={() => {
                            setIsPaused(false)
                            isPausedRef.current = false
                        }}
                    />
                ) : (
                    <PauseButton
                        onClick={() => {
                            setIsPaused(true)
                            isPausedRef.current = true
                        }}
                    />
                )}
            </div>
            <div className="mt-4 ml-20">
                <SettingsButton
                    onClick={() => settingsInfo.setShowSettings(true)}
                />
            </div>
        </div>
    </div>
)

}

export default Timer
