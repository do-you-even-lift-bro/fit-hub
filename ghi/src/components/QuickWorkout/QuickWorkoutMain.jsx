import 'tailwindcss/tailwind.css'
import Timer from './Timer'
import Settings from './Settings'
import { useState } from 'react'
import SettingsContext from './SettingsContext'
import YoutubeEmbed from './YouTube'
import { Footer } from 'flowbite-react'

function QuickWorkout() {
    const [showSettings, setShowSettings] = useState(false)
    const [workoutMinutes, setWorkoutMinutes] = useState(1)
    const [restMinutes, setRestMinutes] = useState(1)

    return (
        <div
            className="fle-grow bg-body bg-center bg-no-repeat bg-[url('https://i.imgur.com/GwS5hAc.jpeg')] bg-gray-700 bg-blend-multiply text-yellow min-h-screen py-8"
            style={{
                backgroundSize: 'cover',
                backgroundPosition: 'center',
                backgroundAttachment: 'fixed',
            }}
        >
            {' '}
            <div className="flex items-center justify-center -mt-40">
                <SettingsContext.Provider
                    value={{
                        workoutMinutes,
                        restMinutes,
                        setWorkoutMinutes,
                        setRestMinutes,
                        showSettings,
                        setShowSettings,
                    }}
                >
                    <div className="flex h-screen items-center justify-center">
                        {showSettings ? <Settings /> : <Timer />}
                    </div>
                </SettingsContext.Provider>
                <div className="items-center mx-20">
                    <YoutubeEmbed embedId="rKDhP1R7wy0" />
                </div>
            </div>
            <Footer
                container
                className="bg-transparent fixed bottom-0 left-0 w-full"
            >
                <div className="w-full text-center flex flex-col">
                    <Footer.Divider />
                    <div className="flex w-full items-center justify-center">
                        <Footer.Brand
                            src="https://i.imgur.com/Id7WV8w.png"
                            alt="FitHub Logo"
                            name="FitHub"
                        />
                        <Footer.Copyright href="#" by="FitHub" year={2024} />
                    </div>
                </div>
            </Footer>
        </div>
    )
}

export default QuickWorkout
