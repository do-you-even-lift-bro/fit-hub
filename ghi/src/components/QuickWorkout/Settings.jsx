import { useContext } from 'react'
import SettingsContext from './SettingsContext'
import BackButton from './BackButton'

export default function Settings() {
    const settingsInfo = useContext(SettingsContext)

    return (
        <div className="border rounded-lg bg-slate-600 p-4">
            <label
                htmlFor="workout-range"
                className="block mb-2 text-lg font-medium text-white"
            >
                Workout Time: {settingsInfo.workoutMinutes} minutes
            </label>
            <input
                id="workout-range"
                type="range"
                min="0"
                max="5"
                value={settingsInfo.workoutMinutes}
                onChange={(event) =>
                    settingsInfo.setWorkoutMinutes(
                        parseInt(event.target.value, 10)
                    )
                }
                className="w-full h-3 bg-gray-200 rounded-lg appearance-none cursor-pointer range-lg dark:bg-gray-700"
            />
            <label
                htmlFor="rest-range"
                className="block mb-2 text-lg font-medium text-white"
            >
                Rest Time: {settingsInfo.restMinutes} minutes
            </label>
            <input
                id="rest-range"
                type="range"
                min="0"
                max="5"
                value={settingsInfo.restMinutes}
                onChange={(event) =>
                    settingsInfo.setRestMinutes(
                        parseInt(event.target.value, 10)
                    )
                }
                className="w-full h-3 bg-gray-200 rounded-lg appearance-none cursor-pointer dark:bg-gray-700"
            />
            <div style={{ textAlign: 'center', marginTop: '20px' }}>
                <BackButton
                    onClick={() => settingsInfo.setShowSettings(false)}
                />
            </div>
        </div>
    )
}
