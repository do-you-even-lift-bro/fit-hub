export default function Pausebutton(props) {
    return (
        <button
            {...props}
            className="bg-transparent text-white border-0 inline-block w-1/5 cursor-pointer duration-300 hover:text-yellow"
        >
            <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                strokeWidth="1.5"
                stroke="currentColor"
                className="w-30 h-30"
            >
                <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M15.75 5.25v13.5m-7.5-13.5v13.5"
                />
            </svg>
        </button>
    )
}
