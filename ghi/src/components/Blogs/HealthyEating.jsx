import { Link } from 'react-router-dom'
import { Button } from 'flowbite-react'
import { Footer } from 'flowbite-react'

function HealthyEatingBlog() {
    const blogImage = {
        imageUrl: 'https://i.imgur.com/TLZSEPS.png',
    }

    return (
        <div className="bg-body min-h-screen">
            <div className="mx-40 bg-body">
                <div className="mb-8 max-w-2xl mx-auto text-white">
                    <img
                        src={blogImage.imageUrl}
                        alt="Healthy Eating"
                        className="w-full"
                    />
                    <div className="p-4">
                        <h2 className="text-yellow text-center text-xl font-bold mb-2 ">
                            Simplifying Healthy Eating: A Quick Guide
                        </h2>
                        <p className="text-gray-300 max-w-lg mx-auto">
                            Maintaining a healthy eating diet amidst the chaos
                            of daily life need not be daunting. Here is a
                            concise guide to help you adopt nutritious habits
                            without feeling overwhelmed.
                        </p>
                    </div>
                </div>

                <div className="flex flex-col items-center text-white">
                    <div className="flex flex-wrap justify-between mb-8">
                        <div className="w-full md:w-1/2 p-4">
                            <h3 className="text-lg font-bold mb-2">
                                Embrace Whole Foods
                            </h3>
                            <p className="text-gray-300">
                                Focus on incorporating whole, minimally
                                processed foods into your diet. Load up on
                                fruits, vegetables, lean proteins, whole grains,
                                and healthy fats. These nutrient-rich choices
                                keep you satisfied and energized throughout the
                                day.
                            </p>
                        </div>

                        <div className="w-full md:w-1/2 p-4">
                            <h3 className="text-lg font-bold mb-2">
                                Plan and Portion
                            </h3>
                            <p className="text-gray-300">
                                Take time to plan and prepare meals in advance,
                                making healthier options easily accessible.
                                Practice portion control by listening to your
                                hunger cues and balancing your plate with
                                appropriate servings of fruits, vegetables,
                                protein, and grains.
                            </p>
                        </div>

                        <div className="w-full md:w-1/2 p-4">
                            <h3 className="text-lg font-bold mb-2">
                                Mindful Moderation
                            </h3>
                            <p className="text-gray-300">
                                Enjoy your favorite foods in moderation rather
                                than deprivation. Practice mindful indulgence,
                                savoring treats without overindulging. Surround
                                yourself with supportive individuals who share
                                your health goals, providing encouragement and
                                accountability along the way.
                            </p>
                        </div>
                    </div>
                    <Link to="/blogs">
                        <Button className="border border-yellow text-yellow hover:border-white hover:text-yellow mt-4 duration-500">
                            Back to Blogs
                        </Button>
                    </Link>
                </div>
            </div>
            <Footer
                container
                className="bg-transparent fixed bottom-0 left-0 w-full"
            >
                <div className="w-full text-center flex flex-col">
                    <Footer.Divider />
                    <div className="flex w-full items-center justify-center">
                        <Footer.Brand
                            src="https://i.imgur.com/Id7WV8w.png"
                            alt="FitHub Logo"
                            name="FitHub"
                        />
                        <Footer.Copyright href="#" by="FitHub" year={2024} />
                    </div>
                </div>
            </Footer>
        </div>
    )
}

export default HealthyEatingBlog
