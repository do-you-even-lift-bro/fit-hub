import { Link } from 'react-router-dom'
import { Button } from 'flowbite-react'
import { Footer } from 'flowbite-react'

function WorkoutRoutineBlog() {
    const blogImage = {
        imageUrl: 'https://i.imgur.com/btgIY4M.jpeg',
    }

    return (
        <div className="bg-body min-h-screen">
            <div className="mx-40 bg-body">
                <div className="mb-8 max-w-2xl mx-auto text-white">
                    <img
                        src={blogImage.imageUrl}
                        alt="Quick Workout"
                        className="w-full"
                    />
                    <div className="p-4">
                        <h2 className="text-yellow text-center text-xl font-bold mb-2 ">
                            The Ultimate 30-Minute Workout Routine
                        </h2>
                        <p className="text-gray-300 max-w-lg mx-auto">
                            Short on time? No problem! Here is a 30-minute
                            workout routine that will help you get fit and stay
                            healthy without spending hours at the gym.
                        </p>
                    </div>
                </div>

                <div className="flex flex-col items-center text-white">
                    <div className="flex flex-wrap justify-between mb-8">
                        <div className="w-full md:w-1/2 p-4">
                            <h3 className="text-lg font-bold mb-2">
                                Warm-up (5 minutes)
                            </h3>
                            <p className="text-gray-300">
                                Start with a 5-minute warm-up to prepare your
                                body for exercise. Include dynamic stretches,
                                light jogging, or jumping jacks to increase your
                                heart rate and loosen up your muscles.
                            </p>
                        </div>

                        <div className="w-full md:w-1/2 p-4">
                            <h3 className="text-lg font-bold mb-2">
                                Cardio (10 minutes)
                            </h3>
                            <p className="text-gray-300">
                                Next, move on to 10 minutes of cardio exercises
                                such as running, cycling, or jumping rope. This
                                will help improve your cardiovascular health and
                                burn calories.
                            </p>
                        </div>

                        <div className="w-full md:w-1/2 p-4">
                            <h3 className="text-lg font-bold mb-2">
                                Strength Training (10 minutes)
                            </h3>
                            <p className="text-gray-300">
                                Incorporate strength training exercises like
                                squats, lunges, push-ups, and planks for 10
                                minutes to build muscle and increase strength.
                                Use dumbbells or body weight for resistance.
                            </p>
                        </div>

                        <div className="w-full md:w-1/2 p-4">
                            <h3 className="text-lg font-bold mb-2">
                                Cool Down and Stretch (5 minutes)
                            </h3>
                            <p className="text-gray-300">
                                Finish your workout with a 5-minute cool down
                                and stretching session. Focus on stretching the
                                muscles you worked during your workout to
                                improve flexibility and prevent injury.
                            </p>
                        </div>
                    </div>
                    <Link to="/blogs">
                        <Button className="border border-yellow text-yellow hover:border-white hover:text-yellow mt-4 duration-500">
                            Back to Blogs
                        </Button>
                    </Link>
                </div>
            </div>
            <Footer
                container
                className="bg-transparent fixed bottom-0 left-0 w-full"
            >
                <div className="w-full text-center flex flex-col">
                    <Footer.Divider />
                    <div className="flex w-full items-center justify-center">
                        <Footer.Brand
                            src="https://i.imgur.com/Id7WV8w.png"
                            alt="FitHub Logo"
                            name="FitHub"
                        />
                        <Footer.Copyright href="#" by="FitHub" year={2024} />
                    </div>
                </div>
            </Footer>
        </div>
    )
}

export default WorkoutRoutineBlog
