import { Link } from 'react-router-dom'
import { Button } from 'flowbite-react'
import { Footer } from 'flowbite-react'

function BreaksBlog() {
    const blogImage = {
        imageUrl: 'https://i.imgur.com/SRZR1jl.jpeg',
    }

    return (
        <div className="bg-body min-h-screen">
            <div className="mx-40 bg-body">
                <div className="mb-8 max-w-2xl mx-auto text-white">
                    <img
                        src={blogImage.imageUrl}
                        alt="Taking a Break"
                        className="w-full"
                    />
                    <div className="p-4">
                        <h2 className="text-yellow text-center text-xl font-bold mb-2 ">
                            Tips for Taking Breaks from Sitting at Your Computer
                        </h2>
                        <p className="text-gray-300 max-w-lg mx-auto">
                            Sitting at a computer all day can take a toll on
                            your health. Incorporate these tips to take regular
                            breaks and reduce the negative effects of prolonged
                            sitting.
                        </p>
                    </div>
                </div>

                <div className="flex flex-col items-center text-white">
                    <div className="flex flex-wrap justify-between mb-8">
                        <div className="w-full md:w-1/2 p-4">
                            <h3 className="text-lg font-bold mb-2">
                                Set Reminders
                            </h3>
                            <p className="text-gray-300">
                                Use an app or set reminders on your phone or
                                computer to prompt you to take breaks at regular
                                intervals. Aim to take a short break every 30
                                minutes to an hour.
                            </p>
                        </div>

                        <div className="w-full md:w-1/2 p-4">
                            <h3 className="text-lg font-bold mb-2">
                                Stretch Regularly
                            </h3>
                            <p className="text-gray-300">
                                Incorporate stretching exercises into your
                                breaks to relieve muscle tension and improve
                                circulation. Focus on stretching your neck,
                                shoulders, back, and legs.
                            </p>
                        </div>

                        <div className="w-full md:w-1/2 p-4">
                            <h3 className="text-lg font-bold mb-2">
                                Take Active Breaks
                            </h3>
                            <p className="text-gray-300">
                                Instead of sitting during your breaks, try to
                                incorporate some physical activity. Take a short
                                walk, do some light exercise, or even just stand
                                up and stretch.
                            </p>
                        </div>

                        <div className="w-full md:w-1/2 p-4">
                            <h3 className="text-lg font-bold mb-2">
                                Practice the 20-20-20 Rule
                            </h3>
                            <p className="text-gray-300">
                                Every 20 minutes, look away from your screen and
                                focus on something at least 20 feet away for at
                                least 20 seconds. This can help reduce eye
                                strain and prevent computer vision syndrome.
                            </p>
                        </div>
                    </div>
                    <Link to="/blogs">
                        <Button className="border border-yellow text-yellow hover:border-white hover:text-yellow mt-4 duration-500">
                            Back to Blogs
                        </Button>
                    </Link>
                </div>
            </div>
            <Footer
                container
                className="bg-transparent fixed bottom-0 left-0 w-full"
            >
                <div className="w-full text-center flex flex-col">
                    <Footer.Divider />
                    <div className="flex w-full items-center justify-center">
                        <Footer.Brand
                            src="https://i.imgur.com/Id7WV8w.png"
                            alt="FitHub Logo"
                            name="FitHub"
                        />
                        <Footer.Copyright href="#" by="FitHub" year={2024} />
                    </div>
                </div>
            </Footer>
        </div>
    )
}

export default BreaksBlog
