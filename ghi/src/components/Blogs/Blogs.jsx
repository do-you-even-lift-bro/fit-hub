import { Link } from 'react-router-dom'
import { Button, Card } from 'flowbite-react'
import { Footer } from 'flowbite-react'

function BlogTemplate() {
    const featuredArticle = {
        title: 'Simplifying Healthy Eating',
        content:
            "Discover the essentials of maintaining a healthy diet in today's fast-paced world.",
        imageUrl: 'https://i.imgur.com/TLZSEPS.png',
    }

    const sleepBlog = {
        title: 'Importance of Quality Sleep',
        content:
            'Learn why getting enough quality sleep is crucial for your overall health and well-being.',
        imageUrl: 'https://i.imgur.com/tEWIkOM.jpeg',
    }

    const workoutBlog = {
        title: 'The Benefits of Regular Exercise',
        content:
            'Explore the numerous physical and mental health benefits that come with incorporating regular exercise into your routine.',
        imageUrl: 'https://i.imgur.com/btgIY4M.jpeg',
    }

    const breaksBlog = {
        title: 'The Importance of Taking Breaks',
        content:
            'Understand the significance of taking breaks and how it can enhance productivity and prevent burnout.',
        imageUrl: 'https://i.imgur.com/SRZR1jl.jpeg',
    }

    return (
        <div className="bg-body pb-16">
            <div className="mx-auto bg-body">
                <div className="flex flex-col mb-8 max-w-2xl mx-auto text-white">
                    <img
                        src={featuredArticle.imageUrl}
                        alt="Featured Article"
                    />
                    <div className="p-4 flex flex-col items-center">
                        <h2 className="text-center text-xl font-bold text-yellow mb-2">
                            {featuredArticle.title}
                        </h2>
                        <p className="text-gray-300 max-w-lg mx-auto">
                            {featuredArticle.content}
                        </p>
                        <Link to="/blogs/healthy-eating">
                            <Button className="border border-yellow text-yellow hover:border-white hover:text-yellow mt-4 duration-500">
                                Read More
                            </Button>
                        </Link>
                    </div>
                </div>
                <div className="flex flex-col items-center text-white">
                    <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 gap-4 mb-8">
                        <Link to="/blogs/sleep">
                            <Card className="bg-transparent max-w-xs">
                                <img
                                    src={sleepBlog.imageUrl}
                                    alt="Sleep Blog"
                                    className="w-full mb-4"
                                />
                                <div className="p-4">
                                    <h2 className="text-lg font-bold mb-2">
                                        {sleepBlog.title}
                                    </h2>
                                    <p className="text-gray-300 mb-4">
                                        {sleepBlog.content}
                                    </p>
                                    <Button className="border border-yellow text-yellow hover:border-white hover:text-yellow duration-500">
                                        Read More
                                    </Button>
                                </div>
                            </Card>
                        </Link>
                        <Link to="/blogs/workout-routine">
                            <Card className="bg-transparent max-w-xs">
                                <img
                                    src={workoutBlog.imageUrl}
                                    alt="Workout Blog"
                                    className="w-full mb-4"
                                />
                                <div className="p-4">
                                    <h2 className="text-lg font-bold mb-2">
                                        {workoutBlog.title}
                                    </h2>
                                    <p className="text-gray-300 mb-4">
                                        {workoutBlog.content}
                                    </p>
                                    <Button className="border border-yellow text-yellow hover:border-white hover:text-yellow duration-500">
                                        Read More
                                    </Button>
                                </div>
                            </Card>
                        </Link>
                        <Link to="/blogs/breaks">
                            <Card className="bg-transparent max-w-xs">
                                <img
                                    src={breaksBlog.imageUrl}
                                    alt="Breaks Blog"
                                    className="w-full mb-4"
                                />
                                <div className="p-4">
                                    <h2 className="text-lg font-bold mb-2">
                                        {breaksBlog.title}
                                    </h2>
                                    <p className="text-gray-300 mb-4">
                                        {breaksBlog.content}
                                    </p>
                                    <Button className="border border-yellow text-yellow hover:border-white hover:text-yellow duration-500">
                                        Read More
                                    </Button>
                                </div>
                            </Card>
                        </Link>
                    </div>
                </div>
            </div>
            <Footer
                container
                className="bg-transparent bottom-0 left-0 w-full"
            >
                <div className="w-full text-center flex flex-col">
                    <Footer.Divider />
                    <div className="flex w-full items-center justify-center">
                        <Footer.Brand
                            src="https://i.imgur.com/Id7WV8w.png"
                            alt="FitHub Logo"
                            name="FitHub"
                        />
                        <Footer.Copyright href="#" by="FitHub" year={2024} />
                    </div>
                </div>
            </Footer>
        </div>
    )
}

export default BlogTemplate
