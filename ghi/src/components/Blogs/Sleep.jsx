import { Link } from 'react-router-dom'
import { Button } from 'flowbite-react'
import { Footer } from 'flowbite-react'

function SleepBlog() {
    const blogImage = {
        imageUrl: 'https://i.imgur.com/tEWIkOM.jpeg',
    }

    return (
        <div className="bg-body min-h-screen">
            <div className="mx-40 bg-body">
                <div className="mb-8 max-w-2xl mx-auto text-white">
                    <img
                        src={blogImage.imageUrl}
                        alt="Sleeping Habits"
                        className="w-full"
                    />
                    <div className="p-4">
                        <h2 className="text-yellow text-center text-xl font-bold mb-2 ">
                            Tips for Developing Healthy Sleeping Habits
                        </h2>
                        <p className="text-gray-300 max-w-lg mx-auto">
                            Sleep is crucial for overall health and well-being.
                            Follow these tips to develop healthy sleeping habits
                            and improve the quality of your sleep.
                        </p>
                    </div>
                </div>

                <div className="flex flex-col items-center text-white">
                    <div className="flex flex-wrap justify-between mb-8">
                        <div className="w-full md:w-1/2 p-4">
                            <h3 className="text-lg font-bold mb-2">
                                Stick to a Schedule
                            </h3>
                            <p className="text-gray-300">
                                Go to bed and wake up at the same time every
                                day, even on weekends. This helps regulate your
                                body internal clock and improves the quality of
                                your sleep.
                            </p>
                        </div>

                        <div className="w-full md:w-1/2 p-4">
                            <h3 className="text-lg font-bold mb-2">
                                Create a Relaxing Bedtime Routine
                            </h3>
                            <p className="text-gray-300">
                                Develop a relaxing bedtime routine to signal to
                                your body that it is time to wind down. This
                                could include activities like reading, taking a
                                warm bath, or practicing relaxation techniques
                                such as deep breathing or meditation.
                            </p>
                        </div>

                        <div className="w-full md:w-1/2 p-4">
                            <h3 className="text-lg font-bold mb-2">
                                Create a Comfortable Sleep Environment
                            </h3>
                            <p className="text-gray-300">
                                Make sure your bedroom is conducive to sleep by
                                keeping it cool, dark, and quiet. Invest in a
                                comfortable mattress and pillows, and remove any
                                distractions such as electronics or clutter.
                            </p>
                        </div>

                        <div className="w-full md:w-1/2 p-4">
                            <h3 className="text-lg font-bold mb-2">
                                Limit Screen Time Before Bed
                            </h3>
                            <p className="text-gray-300">
                                Avoid using electronic devices such as
                                smartphones, tablets, and computers before
                                bedtime. The blue light emitted by these devices
                                can interfere with your body&apos;s natural
                                sleep-wake cycle and make it harder to fall
                                asleep.
                            </p>
                        </div>
                    </div>
                    <Link to="/blogs">
                        <Button className="border border-yellow text-yellow hover:border-white hover:text-yellow mt-4 duration-500">
                            Back to Blogs
                        </Button>
                    </Link>
                </div>
            </div>
            <Footer
                container
                className="bg-transparent fixed bottom-0 left-0 w-full"
            >
                <div className="w-full text-center flex flex-col">
                    <Footer.Divider />
                    <div className="flex w-full items-center justify-center">
                        <Footer.Brand
                            src="https://i.imgur.com/Id7WV8w.png"
                            alt="FitHub Logo"
                            name="FitHub"
                        />
                        <Footer.Copyright href="#" by="FitHub" year={2024} />
                    </div>
                </div>
            </Footer>
        </div>
    )
}

export default SleepBlog
