import { useEffect, useState, useCallback } from 'react'
import { useAuthContext } from '@galvanize-inc/jwtdown-for-react'
import { Link } from 'react-router-dom'
import { useUserData } from '../../context/UserProvider'
import { Footer } from 'flowbite-react'

const API_HOST = import.meta.env.VITE_API_HOST

function UserDashboard() {
    const [user, setUser] = useState()
    const { token } = useAuthContext()
    const { userData } = useUserData()

    const fetchUserData = useCallback(async () => {
        try {
            if (token) {
                const userResponse = await fetch(`${API_HOST}/users`, {
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                })
                if (userResponse.ok) {
                    const userData = await userResponse.json()
                    setUser(userData)
                } else {
                    console.error(
                        'Error fetching user data:',
                        userResponse.statusText
                    )
                }
            }
        } catch (error) {
            console.error('Error fetching user data:', error)
        }
    }, [token])

    useEffect(() => {
        fetchUserData()
    }, [token, fetchUserData])
    if (!userData) {
        return null
    }
    return (
        <div className="flex flex-col min-h-screen">
            <div
                className="flex-grow bg-body bg-center bg-no-repeat bg-[url('https://i.imgur.com/GwS5hAc.jpeg')] bg-gray-700 bg-blend-multiply text-yellow min-h-screen py-8"
                style={{
                    backgroundSize: 'cover',
                    backgroundPosition: 'center',
                    backgroundAttachment: 'fixed',
                }}
            >
                <div className="max-w-4xl mx-auto px-4">
                    <div className="text-center">
                        {user ? (
                            <h1 className="text-3xl font-bold">
                                Welcome {userData.first_name}{' '}
                                {userData.last_name}
                                !💪
                            </h1>
                        ) : (
                            <h1 className="text-3xl font-bold">Loading...</h1>
                        )}
                    </div>
                    <div className="mt-8">
                        <h2 className="text-xl-yellow font-semibold">
                            Your Dashboard
                        </h2>
                        <div className="grid grid-cols-1 md:grid-cols-3 gap-4 mt-4">
                            <div className="bg-yellow text-gray-500 shadow rounded-lg p-4 flex flex-col items-center justify-center">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    fill="none"
                                    viewBox="0 0 24 24"
                                    strokeWidth={1.5}
                                    stroke="currentColor"
                                    className="w-6 h-6"
                                >
                                    <path
                                        strokeLinecap="round"
                                        strokeLinejoin="round"
                                        d="M3 13.125C3 12.504 3.504 12 4.125 12h2.25c.621 0 1.125.504 1.125 1.125v6.75C7.5 20.496 6.996 21 6.375 21h-2.25A1.125 1.125 0 0 1 3 19.875v-6.75ZM9.75 8.625c0-.621.504-1.125 1.125-1.125h2.25c.621 0 1.125.504 1.125 1.125v11.25c0 .621-.504 1.125-1.125 1.125h-2.25a1.125 1.125 0 0 1-1.125-1.125V8.625ZM16.5 4.125c0-.621.504-1.125 1.125-1.125h2.25C20.496 3 21 3.504 21 4.125v15.75c0 .621-.504 1.125-1.125 1.125h-2.25a1.125 1.125 0 0 1-1.125-1.125V4.125Z"
                                    />
                                </svg>
                                <p className="text-base">
                                    {' '}
                                    <Link
                                        to="/userlist"
                                        className="hover:text-black-500"
                                    >
                                        Your Progress{' '}
                                    </Link>
                                </p>
                            </div>
                            <div className="bg-yellow text-gray-500 shadow rounded-lg p-4 flex flex-col items-center justify-center">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 24 24"
                                    fill="currentColor"
                                    className="w-6 h-6 mb-2"
                                >
                                    <path
                                        fillRule="evenodd"
                                        d="M5.166 2.621v.858c-1.035.148-2.059.33-3.071.543a.75.75 0 0 0-.584.859 6.753 6.753 0 0 0 6.138 5.6 6.73 6.73 0 0 0 2.743 1.346A6.707 6.707 0 0 1 9.279 15H8.54c-1.036 0-1.875.84-1.875 1.875V19.5h-.75a2.25 2.25 0 0 0-2.25 2.25c0 .414.336.75.75.75h15a.75.75 0 0 0 .75-.75 2.25 2.25 0 0 0-2.25-2.25h-.75v-2.625c0-1.036-.84-1.875-1.875-1.875h-.739a6.706 6.706 0 0 1-1.112-3.173 6.73 6.73 0 0 0 2.743-1.347 6.753 6.753 0 0 0 6.139-5.6.75.75 0 0 0-.585-.858 47.077 47.077 0 0 0-3.07-.543V2.62a.75.75 0 0 0-.658-.744 49.22 49.22 0 0 0-6.093-.377c-2.063 0-4.096.128-6.093.377a.75.75 0 0 0-.657.744Zm0 2.629c0 1.196.312 2.32.857 3.294A5.266 5.266 0 0 1 3.16 5.337a45.6 45.6 0 0 1 2.006-.343v.256Zm13.5 0v-.256c.674.1 1.343.214 2.006.343a5.265 5.265 0 0 1-2.863 3.207 6.72 6.72 0 0 0 .857-3.294Z"
                                        clipRule="evenodd"
                                    />
                                </svg>
                                <p className="text-base">
                                    {' '}
                                    <Link
                                        to="/workoutlist"
                                        className="hover:text-black-500"
                                    >
                                        Your Personalized Workout Plan{' '}
                                    </Link>
                                </p>
                            </div>
                            <div className="bg-yellow text-gray-500 shadow rounded-lg p-4 flex flex-col items-center justify-center">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 24 24"
                                    fill="currentColor"
                                    className="w-6 h-6 mb-2"
                                >
                                    <path
                                        fillRule="evenodd"
                                        d="M2.25 12c0-5.385 4.365-9.75 9.75-9.75s9.75 4.365 9.75 9.75-4.365 9.75-9.75 9.75S2.25 17.385 2.25 12Zm13.36-1.814a.75.75 0 1 0-1.22-.872l-3.236 4.53L9.53 12.22a.75.75 0 0 0-1.06 1.06l2.25 2.25a.75.75 0 0 0 1.14-.094l3.75-5.25Z"
                                        clipRule="evenodd"
                                    />
                                </svg>
                                <p className="text-base">
                                    {' '}
                                    <Link
                                        to="/meals"
                                        className="hover:text-black-500"
                                    >
                                        Your Personalized Meal Plan{' '}
                                    </Link>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <Footer
                    container
                    className="bg-transparent fixed bottom-0 left-0 w-full"
                >
                    <div className="w-full text-center flex flex-col">
                        <Footer.Divider />
                        <div className="flex w-full items-center justify-center">
                            <Footer.Brand
                                src="https://i.imgur.com/Id7WV8w.png"
                                alt="FitHub Logo"
                                name="FitHub"
                            />
                            <Footer.Copyright
                                href="#"
                                by="FitHub"
                                year={2024}
                            />
                        </div>
                    </div>
                </Footer>
            </div>
        </div>
    )
}

export default UserDashboard
