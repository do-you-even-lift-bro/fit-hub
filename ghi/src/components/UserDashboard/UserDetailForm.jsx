import { useEffect, useState } from 'react'
import { useAuthContext } from '@galvanize-inc/jwtdown-for-react'
import { useUserData } from '../../context/UserProvider'
import { Footer, Button } from 'flowbite-react'
import { Link } from 'react-router-dom'

function UserDetailForm() {
    const API_HOST = import.meta.env.VITE_API_HOST
    const { token } = useAuthContext()
    const { userData } = useUserData()
    const [userDetail, setUserDetail] = useState({
        user_id: '',
        weight: '',
        progress_pic: '',
        created_at: new Date().toISOString().slice(0, 10),
    })

    const handleInputChange = (e) => {
        const { name, value } = e.target
        setUserDetail({
            ...userDetail,
            [name]: value,
        })
    }

    useEffect(() => {
        if (userData && userData.id) {
            setUserDetail((currentDetails) => ({
                ...currentDetails,
                user_id: userData.id,
            }))
        }
    }, [userData])

    const handleSubmit = async (event) => {
        event.preventDefault()
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(userDetail),
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${token}`,
            },
        }
        try {
            const response = await fetch(
                `${API_HOST}/user_details`,
                fetchConfig
            )
            if (response.ok) {
                alert('User detail added successfully!')
            } else {
                console.error('Failed to add user detail:', response.statusText)
                alert('Failed to add user detail')
            }
        } catch (error) {
            console.error('Network error:', error.message)
            alert('Network error')
        }
    }

    if (!userData) {
        return null
    }

    return (
        <div
            className="min-h-screen bg-body bg-center bg-no-repeat bg-[url('https://i.imgur.com/GwS5hAc.jpeg')] bg-gray-700 bg-blend-multiply text-white flex justify-center items-center"
            style={{
                backgroundSize: 'cover',
                backgroundPosition: 'center',
                backgroundAttachment: 'fixed',
            }}
        >
            <div className="bg-gray-800 p-6 rounded-lg shadow-lg w-full max-w-md">
                <h2 className="text-2xl font-bold text-orange-500 mb-4">
                    Track Your Progress
                </h2>
                <form
                    className="flex flex-col space-y-4"
                    onSubmit={handleSubmit}
                >
                    <input
                        type="text"
                        name="user_id"
                        value={userDetail.user_id}
                        onChange={handleInputChange}
                        hidden
                    />
                    <input
                        className="p-2 bg-gray-900 text-black border border-gray-600 rounded"
                        type="number"
                        name="weight"
                        placeholder="Weight (lbs)"
                        value={userDetail.weight}
                        onChange={handleInputChange}
                    />
                    <input
                        className="p-2 bg-gray-900 text-black border border-gray-600 rounded"
                        type="text"
                        name="progress_pic"
                        placeholder="Progress Picture URL"
                        value={userDetail.progress_pic}
                        onChange={handleInputChange}
                    />
                    <input
                        className="p-2 bg-gray-900 text-black border border-gray-600 rounded"
                        type="date"
                        name="created_at"
                        value={userDetail.created_at}
                        onChange={handleInputChange}
                    />
                    <button
                        className="bg-orange-500 hover:bg-orange-600 text-white px-4 py-2 rounded"
                        type="submit"
                    >
                        Add User Detail
                    </button>
                </form>
                <div>
                    <Link to="/userlist">
                        <Button className="border border-yellow text-yellow hover:border-white hover:text-yellow mt-4 duration-500">
                            Back
                        </Button>
                    </Link>
                </div>
            </div>
            <Footer
                container
                className="bg-transparent fixed bottom-0 left-0 w-full"
            >
                <div className="w-full text-center flex flex-col">
                    <Footer.Divider />
                    <div className="flex w-full items-center justify-center">
                        <Footer.Brand
                            src="https://i.imgur.com/Id7WV8w.png"
                            alt="FitHub Logo"
                            name="FitHub"
                        />
                        <Footer.Copyright href="#" by="FitHub" year={2024} />
                    </div>
                </div>
            </Footer>
        </div>
    )
}

export default UserDetailForm
