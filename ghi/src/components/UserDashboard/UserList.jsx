import { useEffect, useState } from 'react'
import { Button } from 'flowbite-react'
import { Link } from 'react-router-dom'
import { useUserData } from '../../context/UserProvider'
import { useAuthContext } from '@galvanize-inc/jwtdown-for-react'
import { Footer } from 'flowbite-react'

function UserList() {
    const API_HOST = import.meta.env.VITE_API_HOST
    const [userDetails, setUserDetails] = useState([])
    const { userData } = useUserData()
    const { token } = useAuthContext()

    useEffect(() => {
        const fetchUserDetails = async () => {
            try {
                const response = await fetch(
                    `${API_HOST}/user_details/${userData.id}`,
                    {
                        headers: {
                            Authorization: `Bearer ${token}`,
                        },
                    }
                )
                if (response.ok) {
                    const data = await response.json()
                    setUserDetails(data)
                } else {
                    console.error('Failed to fetch user details')
                }
            } catch (error) {
                console.error('Error:', error)
            }
        }

        if (!userData || !token) {
            return
        }

        fetchUserDetails()
    }, [API_HOST, userData, token])

    const deleteDetail = async (detailId) => {
        try {
            const response = await fetch(
                `${API_HOST}/user_details/${detailId}`,
                {
                    method: 'DELETE',
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                }
            )
            if (response.ok) {
                setUserDetails((currentDetails) =>
                    currentDetails.filter((detail) => detail.id !== detailId)
                )
            } else {
                console.error('Failed to delete user detail')
            }
        } catch (error) {
            console.error('Error:', error)
        }
    }

    return (
        <div
            className="min-h-screen bg-body bg-center bg-no-repeat bg-[url('https://i.imgur.com/GwS5hAc.jpeg')] bg-gray-700 bg-blend-multiply text-white flex justify-center items-center"
            style={{
                backgroundSize: 'cover',
                backgroundPosition: 'center',
                backgroundAttachment: 'fixed',
            }}
        >
            <div>
                <div className="flex flex-wrap justify-center gap-4">
                    {userData &&
                        userDetails.map((detail) => (
                            <div
                                key={detail.id}
                                className="bg-orange-500 p-4 rounded-lg shadow-md flex flex-col items-center justify-between w-64"
                            >
                                <img
                                    src={
                                        detail.progress_pic ||
                                        'placeholder-image.png'
                                    }
                                    alt="Progress"
                                    className="w-full h-32 object-cover rounded-md mb-4"
                                />
                                <p>Weight: {detail.weight}lbs</p>
                                <p>Created At: {detail.created_at}</p>
                                <button
                                    onClick={() => deleteDetail(detail.id)}
                                    className="mt-2 text-white bg-red-600 hover:bg-red-800 font-medium rounded-lg text-sm px-4 py-2 text-center"
                                >
                                    Delete
                                </button>
                            </div>
                        ))}
                </div>
                <div className="flex justify-center mt-4">
                    <Link to="/myprogress">
                        <Button className="border border-yellow text-yellow hover:border-white hover:text-yellow mt-4 duration-500">
                            Track Your Progress
                        </Button>
                    </Link>
                </div>
            </div>
            <Footer
                container
                className="bg-transparent fixed bottom-0 left-0 w-full"
            >
                <div className="w-full text-center flex flex-col">
                    <Footer.Divider />
                    <div className="flex w-full items-center justify-center">
                        <Footer.Brand
                            src="https://i.imgur.com/Id7WV8w.png"
                            alt="FitHub Logo"
                            name="FitHub"
                        />
                        <Footer.Copyright href="#" by="FitHub" year={2024} />
                    </div>
                </div>
            </Footer>
        </div>
    )
}

export default UserList
