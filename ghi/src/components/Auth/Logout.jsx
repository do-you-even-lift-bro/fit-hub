import { useEffect } from 'react'
import useToken from '@galvanize-inc/jwtdown-for-react'
import { useNavigate } from 'react-router-dom'

function Logout() {
    const { logout } = useToken()
    const navigate = useNavigate()

    useEffect(() => {
        const handleLogout = async () => {
            try {
                await fetch('/logout').then((response) => {
                    if (response.ok) {
                        navigate('/*')
                    } else {
                        console.error('Logout failed')
                    }
                })
            } catch (error) {
                console.error('Logout error:', error)
            }
        }

        handleLogout()
    }, [logout, navigate])

    return null
}

export default Logout
