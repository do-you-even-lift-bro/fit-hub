import { useState } from 'react'
import useToken from '@galvanize-inc/jwtdown-for-react'
import { useNavigate } from 'react-router-dom'
import { Footer } from 'flowbite-react'

const Login = () => {
    const { login } = useToken()
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const navigate = useNavigate()

    const handleSubmit = async (e) => {
        e.preventDefault()
        login(email, password)
        e.target.reset()
        navigate('/dashboard')
    }

    return (
        <section>
            <div
                className="min-h-screen bg-body bg-center bg-no-repeat bg-[url('https://i.imgur.com/GwS5hAc.jpeg')] bg-gray-700 bg-blend-multiply text-white flex justify-center items-center"
                style={{
                    backgroundSize: 'cover',
                    backgroundPosition: 'center',
                    backgroundAttachment: 'fixed',
                }}
            >
                <div className="w-full max-w-md">
                    <h3 className="text-2xl font-bold text-orange-500 mb-4">
                        Login
                    </h3>
                    <form
                        onSubmit={handleSubmit}
                        className="bg-gray-800 p-6 rounded-lg shadow-lg space-y-4"
                    >
                        <input
                            type="email"
                            value={email}
                            placeholder="Enter your email"
                            onChange={(e) => setEmail(e.target.value)}
                            className="p-2 w-full bg-gray-900 text-black border border-gray-600 rounded"
                            required
                        />
                        <input
                            type="password"
                            value={password}
                            placeholder="Password"
                            onChange={(e) => setPassword(e.target.value)}
                            className="p-2 w-full bg-gray-900 text-black border border-gray-600 rounded"
                            required
                        />
                        <button
                            type="submit"
                            className="bg-orange-500 hover:bg-orange-600 text-white px-4 py-2 rounded w-full"
                        >
                            Login
                        </button>
                    </form>
                </div>
            </div>
            <Footer
                container
                className="bg-transparent fixed bottom-0 left-0 w-full"
            >
                <div className="w-full text-center flex flex-col">
                    <Footer.Divider />
                    <div className="flex w-full items-center justify-center">
                        <Footer.Brand
                            src="https://i.imgur.com/Id7WV8w.png"
                            alt="FitHub Logo"
                            name="FitHub"
                        />
                        <Footer.Copyright href="#" by="FitHub" year={2024} />
                    </div>
                </div>
            </Footer>
        </section>
    )
}

export default Login
