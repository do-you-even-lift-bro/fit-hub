import { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import useToken from '@galvanize-inc/jwtdown-for-react'
import { Footer } from 'flowbite-react'

function SignUpForm() {
    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const navigate = useNavigate()
    const { login } = useToken()
    const API_HOST = import.meta.env.VITE_API_HOST
    const handleRegistration = async (e) => {
        e.preventDefault()

        const userData = {
            first_name: firstName,
            last_name: lastName,
            email: email,
            password: password,
        }

        try {
            const response = await fetch(`${API_HOST}/users`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(userData),
            })

            if (response.ok) {
                await login(email, password)
                setFirstName('')
                setLastName('')
                setEmail('')
                setPassword('')
                navigate('/dashboard')
            } else {
                console.error('Registration failed:', await response.text())
            }
        } catch (error) {
            console.error('Network error:', error)
        }
    }

    return (
        <div
            className="min-h-screen bg-body bg-center bg-no-repeat bg-[url('https://i.imgur.com/GwS5hAc.jpeg')] bg-gray-700 bg-blend-multiply text-white flex justify-center items-center"
            style={{
                backgroundSize: 'cover',
                backgroundPosition: 'center',
                backgroundAttachment: 'fixed',
            }}
        >
            <div className="w-full max-w-md">
                <h2 className="text-2xl font-bold text-orange-500 mb-4">
                    Sign Up
                </h2>
                <form
                    onSubmit={handleRegistration}
                    className="bg-gray-800 p-6 rounded-lg shadow-lg space-y-4"
                >
                    <div>
                        <label htmlFor="first_name" className="block">
                            First Name:
                        </label>
                        <input
                            type="text"
                            id="first_name"
                            className="p-2 w-full bg-gray-900 text-black border border-gray-600 rounded"
                            value={firstName}
                            onChange={(e) => setFirstName(e.target.value)}
                            required
                        />
                    </div>
                    <div>
                        <label htmlFor="last_name" className="block">
                            Last Name:
                        </label>
                        <input
                            type="text"
                            id="last_name"
                            className="p-2 w-full bg-gray-900 text-black border border-gray-600 rounded"
                            value={lastName}
                            onChange={(e) => setLastName(e.target.value)}
                            required
                        />
                    </div>
                    <div>
                        <label htmlFor="email" className="block">
                            Email:
                        </label>
                        <input
                            type="email"
                            id="email"
                            className="p-2 w-full bg-gray-900 text-black border border-gray-600 rounded"
                            value={email}
                            onChange={(e) => setEmail(e.target.value)}
                            required
                        />
                    </div>
                    <div>
                        <label htmlFor="password" className="block">
                            Password:
                        </label>
                        <input
                            type="password"
                            id="password"
                            className="p-2 w-full bg-gray-900 text-black border border-gray-600 rounded"
                            value={password}
                            onChange={(e) => setPassword(e.target.value)}
                            required
                        />
                    </div>
                    <button
                        type="submit"
                        className="bg-orange-500 hover:bg-orange-600 text-white px-4 py-2 rounded"
                    >
                        Sign Up
                    </button>
                </form>
            </div>
            <Footer
                container
                className="bg-transparent fixed bottom-0 left-0 w-full"
            >
                <div className="w-full text-center flex flex-col">
                    <Footer.Divider />
                    <div className="flex w-full items-center justify-center">
                        <Footer.Brand
                            src="https://i.imgur.com/Id7WV8w.png"
                            alt="FitHub Logo"
                            name="FitHub"
                        />
                        <Footer.Copyright href="#" by="FitHub" year={2024} />
                    </div>
                </div>
            </Footer>
        </div>
    )
}

export default SignUpForm
