import { useEffect, useState } from 'react'

const API_HOST = import.meta.env.VITE_API_HOST
function DietPlanner() {
    const [newMealPlan, setNewMealPlan] = useState({ title: '' })
    const [mealPlans, setMealPlans] = useState([])
    const [newMeal, setNewMeal] = useState({
        meal_plan_id: '',
        title: '',
        total_calories: '',
        recipe_url: '',
    })

    useEffect(() => {
        const fetchMealPlans = async () => {
            try {
                const response = await fetch(`${API_HOST}/meal_plans/`)
                const data = await response.json()
                if (response.ok) {
                    setMealPlans(data)
                } else {
                    console.error('Failed to fetch meal plans')
                }
            } catch (error) {
                console.error('Error:', error)
            }
        }
        fetchMealPlans()
    }, [])

    const handleAddMeal = async (event) => {
        event.preventDefault()
        const data = {
            ...newMeal,
            total_calories: parseInt(newMeal.total_calories),
        }
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }
        try {
            const response = await fetch(`${API_HOST}/meals/`, fetchConfig)
            if (response.ok) {
                await response.json()
                setNewMeal({
                    meal_plan_id: '',
                    title: '',
                    total_calories: '',
                    recipe_url: '',
                })
                alert('Meal created!')
            } else {
                console.error('Failed to create meal:', response.statusText)
            }
        } catch (error) {
            console.error('Network error:', error.message)
        }
    }

    const handleAddMealPlan = async (event) => {
        event.preventDefault()
        const data = { title: newMealPlan.title }
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }
        try {
            const response = await fetch(`${API_HOST}/meal_plans/`, fetchConfig)
            if (response.ok) {
                const newMealPlanResponse = await response.json()
                setNewMealPlan({ title: '' })
                setMealPlans([...mealPlans, newMealPlanResponse])
                alert('Meal plan created!')
            } else {
                console.error(
                    'Failed to create meal plan:',
                    response.statusText
                )
            }
        } catch (error) {
            console.error('Network error:', error.message)
        }
    }

    return (
        <div className="min-h-screen bg-gray-700 text-white p-4 flex flex-col items-center justify-start">
            <div className="flex gap-10 justify-center w-full">
                <div className="bg-gray-800 p-6 rounded-lg shadow-lg w-full md:w-1/2">
                    <h2 className="text-2xl font-bold text-orange-500 mb-4">
                        Create a Meal Plan
                    </h2>
                    <form
                        onSubmit={handleAddMealPlan}
                        className="flex flex-col space-y-4"
                    >
                        <input
                            className="p-2 bg-gray-900 text-black border border-gray-600 rounded"
                            type="text"
                            placeholder="Meal Plan Title"
                            value={newMealPlan.title}
                            onChange={(e) =>
                                setNewMealPlan({
                                    ...newMealPlan,
                                    title: e.target.value,
                                })
                            }
                        />
                        <button
                            className="bg-orange-500 hover:bg-orange-600 text-white px-4 py-2 rounded"
                            type="submit"
                        >
                            Add Meal Plan
                        </button>
                    </form>
                </div>
                <div className="bg-gray-800 p-6 rounded-lg shadow-lg w-full md:w-1/2">
                    <h2 className="text-2xl font-bold text-orange-500 mb-4">
                        Add a Meal
                    </h2>
                    <form
                        onSubmit={handleAddMeal}
                        className="flex flex-col space-y-4"
                    >
                        <select
                            className="p-2 bg-gray-900 text-white border border-gray-600 rounded"
                            value={newMeal.meal_plan_id}
                            onChange={(e) =>
                                setNewMeal({
                                    ...newMeal,
                                    meal_plan_id: e.target.value,
                                })
                            }
                        >
                            <option value="">Select Meal Plan</option>
                            {mealPlans.map((plan) => (
                                <option key={plan.id} value={plan.id}>
                                    {plan.title}
                                </option>
                            ))}
                        </select>
                        <input
                            className="p-2 bg-gray-900 text-black border border-gray-600 rounded"
                            type="text"
                            placeholder="Title"
                            value={newMeal.title}
                            onChange={(e) =>
                                setNewMeal({
                                    ...newMeal,
                                    title: e.target.value,
                                })
                            }
                        />
                        <input
                            className="p-2 bg-gray-900 text-black border border-gray-600 rounded"
                            type="number"
                            placeholder="Total Calories"
                            value={newMeal.total_calories}
                            onChange={(e) =>
                                setNewMeal({
                                    ...newMeal,
                                    total_calories: e.target.value,
                                })
                            }
                        />
                        <input
                            className="p-2 bg-gray-900 text-black border border-gray-600 rounded"
                            type="text"
                            placeholder="Recipe URL"
                            value={newMeal.recipe_url}
                            onChange={(e) =>
                                setNewMeal({
                                    ...newMeal,
                                    recipe_url: e.target.value,
                                })
                            }
                        />
                        <button
                            className="bg-orange-500 hover:bg-orange-600 text-white px-4 py-2 rounded"
                            type="submit"
                        >
                            Add Meal
                        </button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default DietPlanner
