import { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import { Button } from 'flowbite-react'

const API_HOST = import.meta.env.VITE_API_HOST

function DietList() {
    const [meals, setMeals] = useState([])
    const [mealPlans, setMealPlans] = useState([])
    const [selectedMealPlan, setSelectedMealPlan] = useState('')
    const [editMealId, setEditMealId] = useState(null)
    const [editableMeal, setEditableMeal] = useState({})

    useEffect(() => {
        const fetchData = async () => {
            try {
                const mealPlansResponse = await fetch(`${API_HOST}/meal_plans/`)
                const mealPlansData = await mealPlansResponse.json()
                if (mealPlansResponse.ok) {
                    setMealPlans(mealPlansData)
                }
                const mealsResponse = await fetch(`${API_HOST}/meals/`)
                const mealsData = await mealsResponse.json()
                if (mealsResponse.ok) {
                    setMeals(mealsData)
                }
            } catch (error) {
                console.error('Error fetching data:', error)
            }
        }
        fetchData()
    }, [])

    const handleFilterChange = (event) => {
        setSelectedMealPlan(event.target.value)
    }

    const handleEditChange = (event) => {
        const { name, value } = event.target
        setEditableMeal((prev) => ({ ...prev, [name]: value }))
    }

    const handleEditClick = (mealId) => {
        const mealToEdit = meals.find((meal) => meal.id === mealId)
        setEditMealId(mealId)
        setEditableMeal(mealToEdit)
    }

    const handleSave = async () => {
        try {
            const response = await fetch(`${API_HOST}/meals/${editMealId}`, {
                method: 'PUT',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(editableMeal),
            })
            if (response.ok) {
                const updatedMeal = await response.json()
                setMeals(
                    meals.map((meal) =>
                        meal.id === editMealId ? updatedMeal : meal
                    )
                )
                setEditMealId(null)
                setEditableMeal({})
            }
        } catch (error) {
            console.error('Error saving meal:', error)
        }
    }

    const handleDeleteMeal = async (mealId) => {
        try {
            const response = await fetch(`${API_HOST}/meals/${mealId}`, {
                method: 'DELETE',
            })
            if (response.ok) {
                setMeals(meals.filter((meal) => meal.id !== mealId))
            }
        } catch (error) {
            console.error('Error deleting meal:', error)
        }
    }

    const filteredMeals = selectedMealPlan
        ? meals.filter((meal) => meal.meal_plan_id === Number(selectedMealPlan))
        : meals

    return (
        <div
            className="p-4 bg-body bg-center bg-no-repeat text-white min-h-screen bg-[url('https://i.imgur.com/GwS5hAc.jpeg')] bg-gray-700 bg-blend-multiply"
            style={{
                backgroundSize: 'cover',
                backgroundPosition: 'center',
                backgroundAttachment: 'fixed',
            }}
        >
            <div className="mb-8">
                <h2 className="text-xl font-bold mb-4">Filter by Meal Plan</h2>
                <select
                    value={selectedMealPlan}
                    onChange={handleFilterChange}
                    className="p-2 bg-gray-800 text-white border border-gray-600 rounded"
                >
                    <option value="">All Meal Plans</option>
                    {mealPlans.map((plan) => (
                        <option key={plan.id} value={plan.id}>
                            {plan.title}
                        </option>
                    ))}
                </select>
                <Link to="/mealplanner">
                    <Button className="border border-yellow text-yellow hover:border-white hover:text-yellow mt-4 duration-500">
                        Create a Meal
                    </Button>
                </Link>
            </div>
            <div className="mb-8">
                <h2 className="text-xl font-bold mb-4">Meals</h2>
                <div className="overflow-x-auto">
                    <table className="min-w-full bg-gray-800 rounded-lg overflow-hidden">
                        <thead className="bg-orange-600">
                            <tr>
                                <th className="py-2 px-4 text-left">Title</th>
                                <th className="py-2 px-4 text-left">
                                    Calories
                                </th>
                                <th className="py-2 px-4 text-left">
                                    Recipe URL
                                </th>
                                <th className="py-2 px-4 text-left">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            {filteredMeals.map((meal) => (
                                <tr
                                    key={meal.id}
                                    className="border-b border-gray-700"
                                >
                                    {editMealId === meal.id ? (
                                        <>
                                            <td className="py-2 px-4">
                                                <input
                                                    type="text"
                                                    name="title"
                                                    value={
                                                        editableMeal.title || ''
                                                    }
                                                    onChange={handleEditChange}
                                                    className="bg-gray-700 text-black p-1 rounded"
                                                />
                                            </td>
                                            <td className="py-2 px-4">
                                                <input
                                                    type="number"
                                                    name="total_calories"
                                                    value={
                                                        editableMeal.total_calories ||
                                                        ''
                                                    }
                                                    onChange={handleEditChange}
                                                    className="bg-gray-700 text-black p-1 rounded"
                                                />
                                            </td>
                                            <td className="py-2 px-4">
                                                <input
                                                    type="text"
                                                    name="recipe_url"
                                                    value={
                                                        editableMeal.recipe_url ||
                                                        ''
                                                    }
                                                    onChange={handleEditChange}
                                                    className="bg-gray-700 text-black p-1 rounded"
                                                />
                                            </td>
                                            <td className="py-2 px-4">
                                                <button
                                                    onClick={handleSave}
                                                    className="bg-orange-500 hover:bg-orange-700 text-white font-bold py-1 px-2 rounded mr-2"
                                                >
                                                    Save
                                                </button>
                                                <button
                                                    onClick={() =>
                                                        setEditMealId(null)
                                                    }
                                                    className="bg-gray-500 hover:bg-gray-700 text-white font-bold py-1 px-2 rounded"
                                                >
                                                    Cancel
                                                </button>
                                            </td>
                                        </>
                                    ) : (
                                        <>
                                            <td className="py-2 px-4">
                                                {meal.title}
                                            </td>
                                            <td className="py-2 px-4">
                                                {meal.total_calories}
                                            </td>
                                            <td className="py-2 px-4">
                                                <a
                                                    href={meal.recipe_url}
                                                    target="_blank"
                                                    rel="noopener noreferrer"
                                                >
                                                    View Recipe
                                                </a>
                                            </td>
                                            <td className="py-2 px-4">
                                                <button
                                                    onClick={() =>
                                                        handleEditClick(meal.id)
                                                    }
                                                    className="bg-yellow-500 hover:bg-yellow-700 text-white font-bold py-1 px-2 rounded mr-2"
                                                >
                                                    Edit
                                                </button>
                                                <button
                                                    onClick={() =>
                                                        handleDeleteMeal(
                                                            meal.id
                                                        )
                                                    }
                                                    className="bg-red-500 hover:bg-red-700 text-white font-bold py-1 px-2 rounded"
                                                >
                                                    Delete
                                                </button>
                                            </td>
                                        </>
                                    )}
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    )
}

export default DietList
