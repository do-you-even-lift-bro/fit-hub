import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.jsx'
import { AuthProvider } from '@galvanize-inc/jwtdown-for-react'
import { UserProvider } from './context/UserProvider.jsx'

const API_HOST = import.meta.env.VITE_API_HOST
ReactDOM.createRoot(document.getElementById('root')).render(
    <React.StrictMode>
        <AuthProvider baseUrl={API_HOST}>
            <UserProvider>
                <App />
            </UserProvider>
        </AuthProvider>
    </React.StrictMode>
)
