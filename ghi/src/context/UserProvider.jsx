import {
    useContext,
    createContext,
    useState,
    useEffect,
    useCallback,
    useRef,
} from 'react'
import useToken from '@galvanize-inc/jwtdown-for-react'

const API_HOST = import.meta.env.VITE_API_HOST
const UserContext = createContext()

export function useUserData() {
    return useContext(UserContext)
}

export const UserProvider = ({ children }) => {
    const [userData, setUserData] = useState(null)
    const { fetchWithCookie, token } = useToken()
    const fetchWithCookieRef = useRef(fetchWithCookie)
    const getUserData = useCallback(async () => {
        try {
            const userDataResponse = await fetchWithCookieRef.current(
                `${API_HOST}/token/`
            )
            if (userDataResponse) {
                setUserData(userDataResponse.account)
            }
        } catch (error) {
            console.error('Error fetching user data:', error)
        }
    }, [])

    useEffect(() => {
        fetchWithCookieRef.current = fetchWithCookie
    })

    useEffect(() => {
        if (token) {
            getUserData()
        }
    }, [token, getUserData])
    const value = {
        userData,
    }

    return <UserContext.Provider value={value}>{children}</UserContext.Provider>
}
