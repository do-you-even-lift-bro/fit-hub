import { useState } from 'react'
import useToken from '@galvanize-inc/jwtdown-for-react'
import { useNavigate, NavLink } from 'react-router-dom'

function Navbar() {
    const [isOpen, setIsOpen] = useState(false)
    const { token, logout } = useToken()
    const navigate = useNavigate()

    const handleLogout = async () => {
        try {
            const success = await logout()
            if (success) {
                localStorage.removeItem('token')
                navigate('/login')
            } else {
                console.error('Logout failed')
            }
        } catch (error) {
            console.error('Logout error:', error)
        }
    }

    const toggleMenu = () => {
        setIsOpen(!isOpen)
    }

    const closeMenu = () => {
        setIsOpen(false)
    }
    return (
        <nav
            className="flex justify-between items-center bg-black p-2.5 text-white z-50"
            onMouseLeave={closeMenu}
        >
            <div className="text-xl font-bold flex items-center justify-center">
                <a href="/*" className="inline-block">
                    <img
                        src="https://i.imgur.com/Id7WV8w.png"
                        alt="FitHub Logo"
                        className="w-24 h-auto"
                    />
                </a>
                <span className="text-yellow">Your</span>
                <span className="ml-2">Rules,</span>
                <span className="text-yellow ml-1">Your</span>
                <span className="ml-2">Fitness</span>
            </div>
            <div className="relative z-50">
                <button
                    onClick={toggleMenu}
                    className="menu-button p-2.5 rounded cursor-pointer focus:outline-none flex flex-col justify-center items-center h-10 w-10 bg-black"
                >
                    <div className="block relative w-6 h-0.5 bg-yellow">
                        <span className="absolute inset-0 w-6 h-0.5 bg-yellow transform -translate-y-1.5"></span>
                        <span className="absolute inset-0 w-6 h-0.5 bg-yellow transform translate-y-1.5"></span>
                    </div>
                </button>
                {isOpen && (
                    <div className="dropdown-menu absolute right-5 top-14 bg-black border border-white rounded overflow-hidden z-50 transition-opacity duration-500">
                        <NavLink
                            to="/"
                            className="block px-5 py-2.5 hover:bg-yellow hover:bg-opacity-80 transition-colors duration-300 w-[160px]"
                        >
                            Home
                        </NavLink>
                        {token ? (
                            <>
                                <NavLink
                                    onClick={handleLogout}
                                    className="block px-5 py-2.5 hover:bg-yellow hover:bg-opacity-80 transition-colors duration-300 w-[160px]"
                                    to="/logout"
                                >
                                    Logout
                                </NavLink>
                                <NavLink
                                    to="/dashboard"
                                    className="block px-5 py-2.5 hover:bg-yellow hover:bg-opacity-80 transition-colors duration-300 w-[160px]"
                                >
                                    My Dashboard
                                </NavLink>
                                <NavLink
                                    to="/workoutplanner"
                                    className="block px-5 py-2.5 hover:bg-yellow hover:bg-opacity-80 transition-colors duration-300 w-[160px]"
                                >
                                    Workout Planner
                                </NavLink>
                                <NavLink
                                    to="/myworkouts"
                                    className="block px-5 py-2.5 hover:bg-yellow hover:bg-opacity-80 transition-colors duration-300 w-[160px]"
                                >
                                    My Workouts
                                </NavLink>
                                <NavLink
                                    to="/mealplanner"
                                    className="block px-5 py-2.5 hover:bg-yellow hover:bg-opacity-80 transition-colors duration-300 w-[160px]"
                                >
                                    Meal Planner
                                </NavLink>
                                <NavLink
                                    to="/meals"
                                    className="block px-5 py-2.5 hover:bg-yellow hover:bg-opacity-80 transition-colors duration-300 w-[160px]"
                                >
                                    My Meals
                                </NavLink>
                                <NavLink
                                    to="/blogs"
                                    className="block px-5 py-2.5 hover:bg-yellow hover:bg-opacity-80 transition-colors duration-300 w-[160px]"
                                >
                                    Blogs
                                </NavLink>
                                <NavLink
                                    to="/quickworkout"
                                    className="block px-5 py-2.5 hover:bg-yellow hover:bg-opacity-80 transition-colors duration-300 w-[160px]"
                                >
                                    Quick Workout
                                </NavLink>
                            </>
                        ) : (
                            <>
                                <NavLink
                                    to="/login"
                                    className="block px-5 py-2.5 hover:bg-yellow hover:bg-opacity-80 transition-colors duration-300 w-[160px]"
                                >
                                    Login
                                </NavLink>
                                <NavLink
                                    to="/signup"
                                    className="block px-5 py-2.5 hover:bg-yellow hover:bg-opacity-80 transition-colors duration-300 w-[160px]"
                                >
                                    Sign Up
                                </NavLink>
                                <NavLink
                                    to="/blogs"
                                    className="block px-5 py-2.5 hover:bg-yellow hover:bg-opacity-80 transition-colors duration-300 w-[160px]"
                                >
                                    Blogs
                                </NavLink>
                                <NavLink
                                    to="/quickworkout"
                                    className="block px-5 py-2.5 hover:bg-yellow hover:bg-opacity-80 transition-colors duration-300 w-[160px]"
                                >
                                    Quick Workout
                                </NavLink>
                            </>
                        )}
                    </div>
                )}
            </div>
        </nav>
    )
}

export default Navbar
