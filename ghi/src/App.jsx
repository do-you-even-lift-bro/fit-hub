import { BrowserRouter, Routes, Route } from 'react-router-dom'
import DietPlanner from './components/DietPlanner/DietPlanner'
import Navbar from './Navbar'
import UserDashboard from './components/UserDashboard/UserDashboard'
import Homepage from './components/Homepage/Homepage'
import QuickWorkout from './components/QuickWorkout/QuickWorkoutMain'
import WorkoutPlanner from './components/WorkoutPlanner/WorkoutPlanner'
import Login from './components/Auth/Login'
import Logout from './components/Auth/Logout'
import Blogs from './components/Blogs/Blogs'
import HealthyEatingBlog from './components/Blogs/HealthyEating'
import WorkoutRoutineBlog from './components/Blogs/WorkoutRoutine'
import SleepBlog from './components/Blogs/Sleep'
import BreaksBlog from './components/Blogs/Breaks'
import SignUpForm from './components/Auth/SignUp'
import DietList from './components/DietPlanner/DietList'
import WorkoutList from './components/WorkoutPlanner/WorkoutList'
import UserDetailForm from './components/UserDashboard/UserDetailForm'
import UserList from './components/UserDashboard/UserList'

function App() {
    return (
        <BrowserRouter>
            <Navbar />
            <Routes>
                <Route path="/*" element={<Homepage />} />
                <Route path="/login" element={<Login />} />
                <Route path="/myworkouts" element={<WorkoutList />} />
                <Route path="/logout" element={<Logout />} />
                <Route path="/signup" element={<SignUpForm />} />
                <Route path="/dashboard" element={<UserDashboard />} />
                <Route path="/meals" element={<DietList />} />
                <Route path="/mealplanner" element={<DietPlanner />} />
                <Route path="/workoutplanner" element={<WorkoutPlanner />} />
                <Route path="/quickworkout" element={<QuickWorkout />} />
                <Route path="/blogs" element={<Blogs />} />
                <Route
                    path="/blogs/healthy-eating"
                    element={<HealthyEatingBlog />}
                />
                <Route
                    path="/blogs/workout-routine"
                    element={<WorkoutRoutineBlog />}
                />
                <Route path="/blogs/sleep" element={<SleepBlog />} />
                <Route path="/blogs/breaks" element={<BreaksBlog />} />
                <Route path="/userlist" element={<UserList />} />
                <Route path="/userdetail" element={<UserDetailForm />} />

                <Route path="/workoutlist" element={<WorkoutList />} />
                <Route path="/userlist" element={<UserList />} />
                <Route path="/myprogress" element={<UserDetailForm />} />
            </Routes>
        </BrowserRouter>
    )
}

export default App
